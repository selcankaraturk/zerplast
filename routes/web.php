<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request as req;
use App\Models\UserRole;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => false]);

$restrictScope = function($roles){
    try {
        $result = UserRole::whereIn('role',$roles)->get()->pluck("id")->toArray();
        return implode(',',$result);
    } catch (\Throwable $th) {
        return null;
    }
};

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::middleware('checkRole:'.$restrictScope(['superuser']))->prefix('/superuser')->name('superuser.')->group(function () {
  // Superuser
  Route::get('/', 'Superuser\SuperuserController@index')->name('superuser');

  //Superuser - Settings
  Route::prefix('/settings')->group(function () {

    Route::match(['get','post'],'general', 'Superuser\SettingsController@general')->name('Genel Ayarlar');
    Route::get('newsletter', 'Superuser\SettingsController@newsletter')->name('Bülten İstekleri');

    // Settings - Users
    Route::prefix('users')->group(function(){
        Route::get('/', 'Superuser\SettingsController@users')->name('Kullanıcı Yönetimi');
        Route::match(['post','get'],'/create', 'Superuser\SettingsController@users_create')->name('Yeni Kullanıcı Ekle');
        Route::match(['post','get'],'/edit/{id}', 'Superuser\SettingsController@users_edit')->name('Kullanıcı Düzenle');
    });
    // Superuser - Settings - silme işlemleri
    Route::match(['get','post'],'del', 'Superuser\SettingsController@delete')->name('settings_delete');

  });

  //Superuser - Forms
  Route::prefix('/forms')->group(function () {
    Route::get('/', 'Superuser\FormController@index')->name('Formlar');
    Route::match(['get','post'],'edit/{id}', 'Superuser\FormController@FormEdit')->name('Form Düzenle');
    Route::match(['get','post'],'create', 'Superuser\FormController@FormCreate')->name('Form Oluştur');
  });

  //Superuser - CMS Yönetimi
  Route::prefix('/cms')->group(function () {
    Route::get('/{id}', 'Superuser\CmsController@index')->name('CMS Yönetimi');
    Route::match(['post','get'],'/create/{parent_id}', 'Superuser\CmsController@create')->name('İçerik Oluştur');
    Route::match(['post','get'],'/edit/{id}', 'Superuser\CmsController@edit')->name('İçerik Düzenle');
    Route::match(['post','get'],'/delete/{id}', 'Superuser\CmsController@delete')->name('İçerik Sil');
    Route::match(['post','get'],'/delete/file/{id}', 'Superuser\CmsController@fileDelete')->name('Dosya Sil');
  });


  //Superuser - Language
  Route::prefix('/language')->group(function () {
    Route::get('/', 'Superuser\LanguageController@index')->name('language');
    // Language - create,edit,delete
    Route::match(['get','post'],'create', 'Superuser\LanguageController@create')->name('language_create');
    Route::match(['get','post'],'edit/{id}', 'Superuser\LanguageController@edit')->name('language_edit');
    Route::get('delete/{id}', 'Superuser\LanguageController@delete')->name('language_delete');
    Route::get('initialize/{lang}', 'Superuser\LanguageController@initializeLanguage')->name('language_init');
    Route::get('checklangs', 'Superuser\LanguageController@checkLangs')->name('check languages');
  });

  //Superuser - System
  Route::prefix('/system')->group(function () {
    Route::get('/', 'Superuser\SystemController@index')->name('system');
    //cache clear
    Route::get('/clear/cache', 'Superuser\SystemController@cacheClear')->name('system.clearcache');

    //maintenance mode
    Route::get('/up', 'Superuser\SystemController@systemUp')->name('system.up');
    Route::get('/down', 'Superuser\SystemController@systemDown')->name('system.down');

    //migrate
    Route::get('/migrate', 'Admin\SystemController@databaseMigrate')->name('system.migrate');
  });

});

Route::middleware('checkRole:'.$restrictScope(['superuser','admin']))->prefix('/admin')->group(function () {
    // Admin
    Route::get('/', 'Admin\AdminController@index')->name('admin');

    //Admin - Settings
    Route::prefix('/settings')->group(function () {

      Route::match(['get','post'],'general', 'Admin\SettingsController@general')->name('Genel Ayarlar');
      Route::get('newsletter', 'Admin\SettingsController@newsletter')->name('Bülten İstekleri');

      // Settings - Users
      Route::prefix('users')->group(function(){
          Route::get('/', 'Admin\SettingsController@users')->name('Kullanıcı Yönetimi');
          Route::match(['post','get'],'/create', 'Admin\SettingsController@users_create')->name('Yeni Kullanıcı Ekle');
          Route::match(['post','get'],'/edit/{id}', 'Admin\SettingsController@users_edit')->name('Kullanıcı Düzenle');
      });
      // Admin - Settings - silme işlemleri
      Route::match(['get','post'],'del', 'Admin\SettingsController@delete')->name('settings_delete');

    });

    //Admin - Forms
    Route::prefix('/forms')->group(function () {
      Route::get('/', 'Admin\FormController@index')->name('Formlar');
      Route::match(['get','post'],'edit/{id}', 'Admin\FormController@FormEdit')->name('Form Düzenle');
      Route::match(['get','post'],'create', 'Admin\FormController@FormCreate')->name('Form Oluştur');
    });

    //Admin - CMS Yönetimi
    Route::prefix('/cms')->group(function () {
      Route::get('/{id}', 'Admin\CmsController@index')->name('CMS Yönetimi');
      Route::match(['post','get'],'/create/{parent_id}', 'Admin\CmsController@create')->name('İçerik Oluştur');
      Route::match(['post','get'],'/edit/{id}', 'Admin\CmsController@edit')->name('İçerik Düzenle');
      Route::match(['post','get'],'/delete/{id}', 'Admin\CmsController@delete')->name('İçerik Sil');
      Route::match(['post','get'],'/delete/file/{id}', 'Admin\CmsController@fileDelete')->name('Dosya Sil');
    });


    //Admin - Language
    Route::prefix('/language')->group(function () {
      Route::get('/', 'Admin\LanguageController@index')->name('language');
      // Language - create,edit,delete
      Route::match(['get','post'],'create', 'Admin\LanguageController@create')->name('language_create');
      Route::match(['get','post'],'edit/{id}', 'Admin\LanguageController@edit')->name('language_edit');
      Route::get('delete/{id}', 'Admin\LanguageController@delete')->name('language_delete');
      Route::get('initialize/{lang}', 'Admin\LanguageController@initializeLanguage')->name('language_init');
      Route::get('checklangs', 'Admin\LanguageController@checkLangs')->name('check languages');
    });

    //Admin - System
    Route::prefix('/system')->group(function () {
      Route::get('/', 'Admin\SystemController@index')->name('system');
        //cache clear
        Route::get('/clear/cache', 'Admin\SystemController@cacheClear')->name('system.clearcache');

        //maintenance mode
        Route::get('/up', 'Admin\SystemController@systemUp')->name('system.up');
        Route::get('/down', 'Admin\SystemController@systemDown')->name('system.down');

        //migrate
        Route::get('/migrate', 'Admin\SystemController@databaseMigrate')->name('system.migrate');
    });

  });

// Change Language slug Session
Route::post('/lang/changelang', function (Request $request) {
    $langQuery = DB::table('language')->where('slug',$request->input('change_lang'))->first();
    session(['lang_slug' => $request->input('change_lang'), 'lang_name' => $langQuery->name]);
    return redirect()->back();
})->name('ChangeLang');

// User
Route::middleware('checkRole:'.$restrictScope(['user']))->prefix('/user')->group(function () {
  // User
  Route::get('/', 'User\UserController@index')->name('user');

  //User - CMS Yönetimi
  Route::prefix('/cms')->group(function () {
    Route::get('/{id}', 'User\CmsController@index')->name('user.CMS Yönetimi');
    Route::match(['post','get'],'/create/{parent_id}', 'User\CmsController@create')->name('user.İçerik Oluştur');
    Route::match(['post','get'],'/edit/{id}', 'User\CmsController@edit')->name('user.İçerik Düzenle');
    Route::match(['post','get'],'/delete/{id}', 'User\CmsController@delete')->name('user.İçerik Sil');
    Route::match(['post','get'],'/delete/file/{id}', 'User\CmsController@fileDelete')->name('user.Dosya Sil');
  });

  // Settings - Profile Edit
  Route::match(['post','get'],'profile/edit/', 'User\SettingsController@users_edit')->name('Profil Düzenle');

});

//ajax ve json işlemleri
Route::prefix('/misc')->group(function (){
    Route::get('/json/{name}.json', 'Web\AjaxController@textToJson');
    Route::get('/download/{dosya}', 'Web\BaseController@getDownload')->name('download');
});

Route::get('/', 'Web\BaseController@index');
Route::get('/{slug}', 'Web\BaseController@index');
Route::get('/{language_id}/{slug}', 'Web\BaseController@index');


