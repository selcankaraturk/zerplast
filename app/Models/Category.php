<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CategoryLanguage;

class Category extends Model
{
    protected $table = "category";
    public $timestamps = false;

    public function category_language()
    {
        return $this->hasOne(CategoryLanguage::class,'category_id','id');
    }
}
