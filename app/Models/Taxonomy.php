<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Taxonomy extends Model
{
    protected $table = "taxonomy";
    public $timestamps = false;
    protected $guarded = [];

    public function getUrl()
    {
        return taxonomy($this->category_id,$this->language_slug);
    }
}
