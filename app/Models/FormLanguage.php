<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormLanguage extends Model
{
    use HasFactory;
    protected $table = 'form_language';
    protected $guarded = [];
}
