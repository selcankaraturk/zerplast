<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Extra;

class CategoryLanguage extends Model
{
    protected $table = "category_language";
    public $timestamps = false;
    protected $guarded = [];
    protected $appends = ['extra','taxonomy'];

    public function extras()
    {
        return $this->hasMany(Extra::class,'category_language_id','id');
    }

    public function getExtraAttribute()
    {
        return Extra::where('category_language_id',$this->id)->get()->pluck('value','key');
    }

    public function getTaxonomyAttribute()
    {
        return taxonomy($this->category_id,$this->language_slug);
    }
}
