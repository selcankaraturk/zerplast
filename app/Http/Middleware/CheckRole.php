<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, ...$roles)
    {
        if (!Auth::user()) {
            return redirect('login');
          }else {
            if (in_array(Auth::user()->role,$roles)) {
                return $next($request);
            }else {
                $html = '<body style="margin:0; background-color:#1A1A1A;">
                <div style="width:100%;
                height:100vh;
                display:flex;
                justify-content:center;
                align-items:center;">
                <img src="'.asset("assets/admin/img/yetki.png").'" width="75%">
                </div></body>';
                return response($html, 401);
            }
          }
    }
}
