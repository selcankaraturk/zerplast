<?php

namespace App\Http\Controllers\Superuser;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class FormController extends Controller
{
    public function index()
    {
      $slug = session('lang_slug');
      $lang_name = session('lang_name');

      $formAll = array();
      $forms = DB::table('form')->get();
      foreach ($forms as $key => $form) {

        $form_language = DB::table('form_language')->where('form_id',$form->id)->where('language_slug',$slug)->first();

        $forminfo = array_merge((array)$form, (array)$form_language);

        array_push($formAll,$forminfo);
      }

      return view('superuser.Forms.index',[
        'form_all' => $formAll
      ]);
    }

    public function FormEdit(Request $request,$id)
    {
      $slug = session('lang_slug');
      $lang_name = session('lang_name');

      if ($request->get('del') == true) {
        DB::table('form')->where('id',$id)->delete();
        DB::table('form_language')->where('form_id',$id)->delete();
        return redirect()->back()->with('success','Başarıyla Silindi');
      }

      $form = DB::table('form')->where('id',$id)->first();
      $form_language = DB::table("form_language")->where("form_id",$id)->where("language_slug",$slug)->first();

      if ($request->isMethod('post')) {

        DB::table('form')->where('id',$form_language->form_id)->update([
          'email' => $request->input('email')
        ]);

        DB::table('form_language')->where('form_id',$form_language->form_id)->where('language_slug',$slug)->update([
          'name' => $request->input('name'),
          'description' => $request->input('description')
        ]);

        return redirect()->back()->with('success','Başarıyla Güncellendi');
      }

      return view('superuser.Forms.edit',[
        'form_language' => $form_language,
        'form' => $form
      ]);
    }

    public function FormCreate(Request $request)
    {
      $slug = session('lang_slug');
      $lang_name = session('lang_name');

      if ($request->isMethod('post')) {


        $form_id = DB::table('form')->insertGetId([
          'email' => $request->input('email')
        ]);

        foreach (DB::table('language')->get() as $key => $lang) {
          DB::table('form_language')->insert([
            'form_id' => $form_id,
            'language_slug' => $lang->slug,
            'name' => $request->input('name'),
            'description' => $request->input('description')
          ]);
        }

        return redirect()->back()->with('success','Başarıyla Eklendi');
      }

      return view('superuser.Forms.create');
    }

}
