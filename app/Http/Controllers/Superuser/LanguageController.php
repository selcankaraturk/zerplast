<?php

namespace App\Http\Controllers\Superuser;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\CategoryLanguage;
use App\Models\Taxonomy;

class LanguageController extends Controller
{
    public function index(Request $request)
    {
      $data['languages'] = DB::table('language')->get();
      $lostContents = array();
      foreach ($data['languages'] as $key => $value) {
        $checkDB = DB::table("category_language")
        ->where('language_slug',$value->slug)
        ->doesntExist();
        if($checkDB){
            $lostContents[$key]['slug'] = $value->slug;
            $lostContents[$key]['name'] = $value->name;
        }
      }
      $data['lostContents'] = $lostContents;
      return view('superuser.Language.index',$data);
    }

    public function checkLangs(Request $request)
    {
        //TODO: Listede olmayan fakat veritabanında olan dillerin kontrolü
      $languages = DB::table('language')
                            ->select("slug")
                            ->get()->map(function($value){
                                return $value->slug;
                            });

      return dd($languages);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
          $keys = $request->input('key');
          $dil_adi = $request->input('dil_adi');
          $dil_on_ek = $request->input('on_ek');
          if ($request->hasFile('bayrak')) {
              $bayrak = $request->bayrak->store('');
          }else {
            $bayrak = null;
          }
          $exist = DB::table('language')->where('slug', $dil_on_ek)->exists(); // eğer kayıt varsa 1 değerini döndürecek
          if ($exist != 1) {
              DB::table('language')->insert([
                'name' => $dil_adi,
                'slug' => $dil_on_ek,
                'image' => $bayrak
              ]);
              // Settings tablosuna bir satır ekliyor
              DB::table('settings')->insert([
                'language_id' => $dil_on_ek
              ]);

              //keyler ve degerler
              foreach ($keys as $key => $value) {
                $deger = $request->input("deger")[$key];

                    if (!empty($value)) {

                      DB::table('language_value')->insert([
                        'language_slug' => $dil_on_ek,
                        'language_key' => $value,
                        'language_value' => $deger
                      ]);

                    }
              }

              return redirect()->back()->with('success','Kayıt Başarıyla Eklendi');
          }else {
            return redirect()->back()->with('error','Bu Kayıt Zaten Var!');
          }


        }
        return view('superuser.Language.create');
    }

    public function edit(Request $request,$id)
    {

      if ($request->isMethod('post')) {
        $keys = $request->input('key');
        $dil_adi = $request->input('dil_adi');
        $dil_on_ek = $request->input('on_ek');
        $bayrak_old = DB::table('language')->where("id",$id)->first();
        if ($request->hasFile('bayrak')) {
            Storage::delete($bayrak_old->image);
            $bayrak = $request->bayrak->store('');
        }else {
          $bayrak = $bayrak_old->image;
        }

            DB::table('language')->where('id',$id)->update([
              'name' => $dil_adi,
              'slug' => $dil_on_ek,
              'image' => $bayrak
            ]);

            //keyler ve degerler
            foreach ($keys as $key => $value) {
              $deger = $request->input("deger")[$key];

                  if (!empty($value)) {

                    DB::table('language_value')->updateOrInsert([
                      'id' => $key
                    ],[
                      'language_slug' => $dil_on_ek,
                      'language_key' => $value,
                      'language_value' => $deger
                    ]);

                  }else {
                    DB::table('language_value')->where('id',$key)->delete();
                  }
            }

            return redirect()->back()->with('success','Kayıt Başarıyla Eklendi');



      }

      $data['language'] = DB::table('language')->where("id",$id)->first();
      $data['language_keys'] = DB::table('language_value')->where("language_slug",$data['language']->slug)->get();
      return view('superuser.Language.edit',$data);
    }

    public function delete(Request $request,$id)
    {

      if ($request->get('img')) {
        $bayrak_old = DB::table('language')->where("id",$id);
        Storage::delete($bayrak_old->first()->image);
        $bayrak_old->update([
          'image' => null
        ]);
        return redirect()->back()->with('success','Resim Başarıyla Silindi');
      }else {
        $onlyLang = $request->query('onlylang') ? true : false;
        $query = DB::table('language')->where("id",$id);
        Storage::delete($query->first()->image);
        DB::table('language_value')->where("language_slug",$query->first()->slug)->delete();
        if(!$onlyLang){
            // category_language tablosundaki bu dile ait verileri temizliyoruz
            $cl = DB::table("category_language")->where("language_slug",$query->first()->slug);
            // varsa resimleri de temizliyoruz
            foreach ($cl->get() as $key => $value) {
                if ($value->image) { Storage::delete($value->image); }
                if ($value->image2) { Storage::delete($value->image2); }
                if ($value->image3) { Storage::delete($value->image3); }
            }
            $cl->delete();
        }
        $query->delete();
        return redirect()->back()->with('success','Kayıt Başarıyla Silindi');
      }

    }

    public function initializeLanguage($language_slug){
        $c_count = DB::table('category')->count();
        $cl_count = DB::table('category_language')->where('language_slug',$language_slug)->count();
        if($cl_count == $c_count){
            return response()->json([
                "c_count" => $c_count,
                "cl_count" => $cl_count,
                "isNew" => false,
                "status" => true
            ]);
        }else{
            $category = DB::table('category')->get();
            $settings = DB::table('settings')->where('default_lang',1)->first();
            $default_language = $settings->language_id;
            foreach ($category as $key => $value) {

                //Category_language
                $recordDoesntExist = DB::table("category_language")
                                    ->where("category_id",$value->id)
                                    ->where("language_slug",$language_slug)
                                    ->doesntExist();
                if($recordDoesntExist){
                    $record = CategoryLanguage::where("category_id",$value->id)
                            ->where("language_slug",$default_language)
                            ->first();
                    $new_record = $record->replicate()->fill([
                        "language_slug" => $language_slug
                    ]);

                    // taxonomy
                    $tax = Taxonomy::where("category_id",$value->id)
                    ->where("language_slug",$default_language)
                    ->first();
                    $new_tax = $tax->replicate()->fill([
                        "language_slug" => $language_slug
                    ]);

                    //save
                    $new_record->save();
                    $new_tax->save();
                }
            }
            return response()->json([
                "isNew" => true,
                "status" => true
            ]);
        }
    }
}
