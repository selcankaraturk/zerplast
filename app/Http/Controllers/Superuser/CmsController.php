<?php

namespace App\Http\Controllers\Superuser;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\Extra;
use App\Models\Category;
use App\Models\Form;

class CmsController extends Controller
{
    public function index(Request $request,$id)
    {
      $slug = session('lang_slug');
      $lang_name = session('lang_name');

      $cms = DB::select("select * from category as c inner join category_language as cl on c.id = cl.category_id and c.parent_id='$id' and cl.language_slug='$slug' order by c.sorted ASC");
      $category = DB::table('category')->where('id',$id)->first();
      return view('superuser.Cms.index',[
        'cms' => $cms,
        'lang_name' => $lang_name,
        'category' => $category
      ]);
    }

    public function create(Request $request,$parent_id)
    {
      $slug = session('lang_slug');
      $lang_name = session('lang_name');
      $current_time = \Carbon\Carbon::now()->timestamp;


      if ($request->isMethod('post')) {
        if ($request->input('top_menu')) {
          $top_menu = $request->input('top_menu');
        }else {
          $top_menu = 0;
        }

        if ($request->input('home_page')) {
          $home_page = $request->input('home_page');
        }else {
          $home_page = 0;
        }

        $category_image = $request->input('category_image');
        $category_image2 = $request->input('category_image2');
        $category_image3 = $request->input('category_image3');

        $language_image = $request->input('language_image');
        $language_image2 = $request->input('language_image2');
        $language_image3 = $request->input('language_image3');

        $category_id = DB::table('category')->insertGetId([
          'image' => $category_image,
          'image2' => $category_image2,
          'image3' => $category_image3,
          'sorted' => $request->input('sorted'),
          'create_time' => $current_time,
          'block_id' => $request->input('category'),
          'form_id' => $request->input('form_id'),
          'status' => $request->input('status'),
          'parent_id' => $parent_id,
          'top_menu' => $top_menu,
          'url' => $request->input('url'),
          'home_page' => $home_page
        ]);

        $controller = DB::table('special_blocks')->where('id',$request->input('category'))->first()->block_key;
        $name_slug = str_slug($request->input('name'),'-');
        foreach (DB::table('language')->get() as $key => $language) {
          $cl_id = DB::table('category_language')->insertGetId([
            'category_id' => $category_id,
            'language_slug' => $language->slug,
            'name' => $request->input('name'),
            'image' => $language_image,
            'image2' => $language_image2,
            'image3' => $language_image3,
            'seo_title' => $request->input('seo_title'),
            'seo_description' => $request->input('seo_description'),
            'seo_keywords' => $request->input('seo_keywords'),
            'description' => $request->input('description'),
            'contents' => $request->input('contents'),
            'special_fields' => json_encode($request->input('ozelalan'))
          ]);

          if($request->input('extra')){
                foreach ($request->input('extra') as $key => $value) {
                    Extra::updateOrCreate(
                        ['category_language_id' => $cl_id, 'key' => $key],
                        ['value' => $value]
                    );
            }
            }

          if (DB::table('taxonomy')->where('slug',$name_slug)->where('language_slug',$language->slug)->exists()) {
            $count = $category_id;
          }else {
            $count = null;
          }
          DB::table('taxonomy')->insert([
            'category_id' => $category_id,
            'slug' => $name_slug,
            'language_slug' => $language->slug,
            'count' => $count,
            'controller' => $controller
          ]);
        }

        return redirect()->route('superuser.CMS Yönetimi', ['id' => $parent_id])->with('success','Başarıyla Kaydedildi.');

      }

      $kategoriler = DB::table('special_blocks')->get();
      return view('superuser.Cms.create',[
        'kategoriler' => $kategoriler,
        'parent_id' => $parent_id
      ]);
    }

    public function edit(Request $request,$id)
    {
      $slug = session('lang_slug');
      $lang_name = session('lang_name');
      $current_time = \Carbon\Carbon::now()->timestamp;

      if ($request->isMethod('post')) {
        if ($request->input('top_menu')) {
          $top_menu = $request->input('top_menu');
        }else {
          $top_menu = 0;
        }

        if ($request->input('home_page')) {
          $home_page = $request->input('home_page');
        }else {
          $home_page = 0;
        }

        $category_image = $request->input('category_image');
        $category_image2 = $request->input('category_image2');
        $category_image3 = $request->input('category_image3');

        $language_image = $request->input('language_image');
        $language_image2 = $request->input('language_image2');
        $language_image3 = $request->input('language_image3');

        DB::table('category')->where('id',$id)->update([
          'image' => $category_image,
          'image2' => $category_image2,
          'image3' => $category_image3,
          'sorted' => $request->input('sorted'),
          'update_time' => $current_time,
          'block_id' => $request->input('category'),
          'form_id' => $request->input('form_id'),
          'status' => $request->input('status'),
          'top_menu' => $top_menu,
          'url' => $request->input('url'),
          'home_page' => $home_page
        ]);

        $controller = DB::table('special_blocks')->where('id',$request->input('category'))->first()->block_key;
        $name_slug = str_slug($request->input('name'),'-');

        $cl = DB::table('category_language')->where('category_id',$id)->where("language_slug",$slug);
        $cl->update([
        'name' => $request->input('name'),
        'image' => $language_image,
        'image2' => $language_image2,
        'image3' => $language_image3,
        'seo_title' => $request->input('seo_title'),
        'seo_description' => $request->input('seo_description'),
        'seo_keywords' => $request->input('seo_keywords'),
        'description' => $request->input('description'),
        'contents' => $request->input('contents'),
        'special_fields' => json_encode($request->input('ozelalan'))
        ]);

        if($request->input('extra')){
            foreach ($request->input('extra') as $key => $value) {
                Extra::updateOrCreate(
                    ['category_language_id' => $cl->first()->id, 'key' => $key],
                    ['value' => $value]
                );
          }
        }
          $tax = DB::table('taxonomy')->where('slug',$name_slug)->where('language_slug',$slug);
          if ($tax->exists()) {
            if ($tax->first()->category_id == $id) {
              $count = null;
            }else {
              $count = $id;
            }
          }else {
            $count = null;
          }
          DB::table('taxonomy')->where('category_id',$id)->where('language_slug',$slug)->update([
            'slug' => $name_slug,
            'count' => $count,
            'controller' => $controller
          ]);

        $parent_id = DB::table('category')->where('id',$id)->first()->parent_id;
        return redirect()->route('superuser.CMS Yönetimi', ['id' => $parent_id])->with('success','Başarıyla Kaydedildi.');

      }

      //$category = DB::select("select * from category as c inner join category_language as cl on c.id=cl.category_id where c.id='$id' and cl.language_slug='$slug'");
      $category = Category::with(['category_language' => function($q) use($slug){
        $q->where('language_slug',$slug);
      }])
      ->find($id);
      $taxonomy = DB::table("taxonomy")->where('category_id',$category->id)->where("language_slug",$slug)->first();
      return view('superuser.Cms.edit',[
        'category' => $category,
        'taxonomy' => $taxonomy
      ]);
    }

    public function delete(Request $request, $id)
    {
      $category_id = $id;

      $ca = DB::table('category')->where('id',$category_id);
      $ca_lang = DB::table('category_language')->where('category_id',$category_id);
      $ca_tax = DB::table('taxonomy')->where('category_id',$category_id);

      if ($ca->first()->image) { Storage::delete($ca->first()->image); }
      if ($ca->first()->image2) { Storage::delete($ca->first()->image2); }
      if ($ca->first()->image3) { Storage::delete($ca->first()->image3); }
      $ca->delete();
      foreach ($ca_lang->get() as $key => $value) {
        if ($value->image) { Storage::delete($value->image); }
        if ($value->image2) { Storage::delete($value->image2); }
        if ($value->image3) { Storage::delete($value->image3); }
      }
      $ca_lang->delete();
      $ca_tax->delete();

      $loop = true;
      $myArr = array();
      $myArr[0] = $category_id;
      while ($loop) {

        $isExist = DB::table('category')->whereIn('parent_id',$myArr);
        $myArr = array();
        if ($isExist->exists()) {

          foreach ($isExist->get() as $key => $value) {
            array_push($myArr,$value->id);

            $ca_lang = DB::table('category_language')->where('category_id',$value->id);
            $ca_tax = DB::table('taxonomy')->where('category_id',$value->id);

            if ($value->image) { Storage::delete($value->image); }
            if ($value->image2) { Storage::delete($value->image2); }
            if ($value->image3) { Storage::delete($value->image3); }

            foreach ($ca_lang->get() as $key => $value) {
              if ($value->image) { Storage::delete($value->image); }
              if ($value->image2) { Storage::delete($value->image2); }
              if ($value->image3) { Storage::delete($value->image3); }
            }
            $ca_lang->delete();
            $ca_tax->delete();
          }

          $isExist->delete();

        }else {
          $loop = false;
        }

      }

      return redirect()->back()->with('success','İçerik Tüm Alt İçerikleriyle Birlikte Başarıyla Silindi.');
    }

    public function fileDelete(Request $request, $id)
    {
      $db = $request->query('db');
      $section = $request->query('section');

      if ($db == 'c') {
      $img = DB::table('category')->where('id',$id);
      $img_info = json_decode($img->first()->image_info,true);
      //unset($img_info[$section]);
      $img_info[$section] = array_map(function($val){
        $val = null;
      },$img_info[$section]);
      Storage::delete($img->first()->$section);
      $img->update([
        $section => null,
        'image_info' => json_encode($img_info)
      ]);
      return redirect()->back()->with('success','Dosya Başarıyla Silindi.');
      }elseif ($db == 'cl') {
        $img = DB::table('category_language')->where('id',$id);
        $img_info = json_decode($img->first()->image_info,true);
        //unset($img_info[$section]);
        $img_info[$section] = array_map(function($val){
          $val = null;
        },$img_info[$section]);
        Storage::delete($img->first()->$section);
        DB::table('category_language')->where('id',$id)->update([
          $section => null,
          'image_info' => json_encode($img_info)
        ]);
        return redirect()->back()->with('success','Dosya Başarıyla Silindi.');
      }

    }
}
