<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Taxonomy;


class BaseController extends Controller
{
    public function index(Request $request)
    {
      $language_slug = $request->language_id;
      $slug = $request->slug;
      if (empty($language_slug)) {
        $language_slug = getDefaultLanguage();
      }

        $slugArr = explode('-',$slug);
        $lastElement = end($slugArr);
        $getTaxonomy = Taxonomy::where('slug',$slug)
                        ->where('language_slug',$language_slug)
                        ->first();
        if(!$getTaxonomy and is_numeric($lastElement)){
            $nSlug = str_replace('-'.$lastElement,'',$slug);
            $getTaxonomy = Taxonomy::where('slug',$nSlug)
                    ->where('count',$lastElement)
                    ->where('language_slug',$language_slug)
                    ->first();
        }
      //$getTaxonomy = DB::select("select * from taxonomy where CONCAT_WS('-',slug,count) = '$slug' and language_slug = '$language_slug'");
      if ($getTaxonomy) {

        return \App::call('App\Http\Controllers\Api\\'.$getTaxonomy->controller, [
          'slug' => $slug,
          'language_slug' => $language_slug,
          'tax' => $getTaxonomy
        ]);

      }else {
        if (empty($slug)) {
          return view('web.index.index',[
            'language_slug' => $language_slug
          ]);
        }else {
          if (DB::table('language')->where('slug',$slug)->doesntExist()) {
            abort(404);
          }else {
            return view('web.index.index',[
              'language_slug' => $slug
            ]);
          }
        }
      }

    }
}
