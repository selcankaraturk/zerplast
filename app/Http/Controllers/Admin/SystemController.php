<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Artisan;

class SystemController extends Controller
{
    public function index(Request $request)
    {
        $system['maintenance'] = $this->checkMaintenance();
        return view('admin.System.index',$system);
    }

    public function cacheClear(){
        try {
            $caches = array("cache:clear","route:clear","config:clear","view:clear");
            foreach ($caches as $key => $value) {
                Artisan::call($value);
            }
            return response()->json([
                'status' => 'ok'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'failed',
                'error' => $th->getMessage()
            ]);
        }
    }

    public function databaseMigrate()
    {
        try {
            $artisan = Artisan::call('migrate');
            return response()->view('info.index',[
                'title' => 'Database Migration',
                'message' => 'Migrated!',
                'info' => 'Artisan: '.$artisan
            ]);
        } catch (\Throwable $th) {
            return response($th->getMessage(),500);
        }
    }

    public function systemUp()
    {
        try {
            Artisan::call('up');
            return response()->json([
                'status' => 'ok'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'failed',
                'error' => $th->getMessage()
            ]);
        }
    }

    public function systemDown(Request $request)
    {
        $message = $request->has('message') ? $request->input('message') : null;
        try {
            Artisan::call('down --message='.$message.' --retry=60');
            return response()->json([
                'status' => 'ok'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'failed',
                'error' => $th->getMessage()
            ]);
        }
    }

    public function checkMaintenance()
    {
        if(file_exists(storage_path().'/framework/down')){
            return true; //down
        }else{
            return false; //up
        }
    }
}
