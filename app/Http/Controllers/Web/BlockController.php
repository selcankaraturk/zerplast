<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BlockController extends Controller
{
    public function block(Request $request,$slug,$language_slug,$tax)
    {
        //Http 204 Status Code : No Content
        return abort(204);
    }
}
