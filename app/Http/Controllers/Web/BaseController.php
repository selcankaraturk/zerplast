<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Category;
use http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Taxonomy;
use Illuminate\Support\Facades\App;

class BaseController extends Controller
{
    public function index(Request $request)
    {
        $language_slug = $request->language_id;
        $slug = $request->slug;
        if (empty($language_slug)) {
            $language_slug = getDefaultLanguage();
        }
        $slugArr = explode('-',$slug);
        $lastElement = end($slugArr);
        $getTaxonomy = Taxonomy::where('slug',$slug)
                        ->where('language_slug',$language_slug)
                        ->first();
        if(!$getTaxonomy and is_numeric($lastElement)){
            $nSlug = str_replace('-'.$lastElement,'',$slug);
            $getTaxonomy = Taxonomy::where('slug',$nSlug)
                    ->where('count',$lastElement)
                    ->where('language_slug',$language_slug)
                    ->first();
        }

      if ($getTaxonomy) {
        if(!$request->session()->has('language_slug') and session('language_slug') !== $language_slug){
            session(['language_slug' => $language_slug]);
        }
        return App::call('App\Http\Controllers\Web\\'.$getTaxonomy->controller, [
          'slug' => $slug,
          'language_slug' => $language_slug,
          'tax' => $getTaxonomy
        ]);

      }else {
        if (empty($slug)) {
            if(!$request->session()->has('language_slug') and session('language_slug') !== $language_slug){
                session(['language_slug' => $language_slug]);
            }
            return view('web.index.index',[
                'language_slug' => $language_slug
            ]);
        }else {
          if (DB::table('language')->where('slug',$slug)->doesntExist()) {
            abort(404);
          }else {
            if(!$request->session()->has('language_slug') and session('language_slug') !== $slug){
                session(['language_slug' => $slug]);
            }
            return view('web.index.index',[
              'language_slug' => $slug
            ]);
          }
        }
      }

    }

    public function getDownload($dosya){

        //PDF file is stored under project/public/download/info.pdf
        $file="uploads/files/1/".$dosya;
        return \Response::download($file);
    }
}
