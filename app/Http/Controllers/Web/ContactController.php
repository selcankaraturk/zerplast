<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class ContactController extends Controller
{
    public function index(Request $request,$slug,$language_slug,$tax)
    {
        $category = Category::with([
            'category_language' => function($q) use($language_slug){
                $q->where('language_slug',$language_slug);
            }
        ])->find($tax->category_id);

        return view('web.pages.contact',[
            'slug' => $slug,
            'language_slug' => $language_slug,
            'page' => $category
        ]);
    }
}
