<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class PagesController extends Controller
{
    public function index(Request $request,$slug,$language_slug,$tax)
    {
        $category = Category::with([
            'category_language' => function($q) use($language_slug){
                $q->where('language_slug',$language_slug);
            }
        ])->find($tax->category_id);

        return view('web.pages.index',[
            'slug' => $slug,
            'language_slug' => $language_slug,
            'page' => $category
        ]);
    }

    public function about(Request $request,$slug,$language_slug,$tax)
    {
        $category = Category::with([
            'category_language' => function($q) use($language_slug){
                $q->where('language_slug',$language_slug);
            }
        ])->find($tax->category_id);

        return view('web.pages.about',[
            'slug' => $slug,
            'language_slug' => $language_slug,
            'page' => $category
        ]);
    }

    public function products(Request $request,$slug,$language_slug,$tax)
    {
        $category = Category::with([
            'category_language' => function($q) use($language_slug){
                $q->where('language_slug',$language_slug);
            }
        ])->find($tax->category_id);

        return view('web.pages.products',[
            'slug' => $slug,
            'language_slug' => $language_slug,
            'page' => $category
        ]);
    }

    public function product_detail(Request $request,$slug,$language_slug,$tax)
    {
        $category = Category::with([
            'category_language' => function($q) use($language_slug){
                $q->where('language_slug',$language_slug);
            }
        ])->find($tax->category_id);
        $parent = Category::find($category->parent_id);

        $Id = Category::where('parent_id',$parent->parent_id)->select('id')->pluck('id');

        $smilarProducts = Category::with(['category_language'=>function ($q) use($language_slug){
            $q->where('language_slug',$language_slug);
        }])->whereIn('parent_id',$Id)->get();

        return view('web.pages.product_detail',[
            'slug' => $slug,
            'language_slug' => $language_slug,
            'page' => $category,
            'smilarProducts' => $smilarProducts
        ]);
    }

    public function blog(Request $request,$slug,$language_slug,$tax)
    {
        $category = Category::with([
            'category_language' => function($q) use($language_slug){
                $q->where('language_slug',$language_slug);
            }
        ])->find($tax->category_id);

        return view('web.pages.blog',[
            'slug' => $slug,
            'language_slug' => $language_slug,
            'page' => $category
        ]);
    }

    public function blog_detail(Request $request,$slug,$language_slug,$tax)
    {
        $category = Category::with([
            'category_language' => function($q) use($language_slug){
                $q->where('language_slug',$language_slug);
            }
        ])->find($tax->category_id);

        $lastBlog = Category::with([
            'category_language' => function($q) use($language_slug){
                $q->where('language_slug',$language_slug);
            }
        ])->where('parent_id', 5)->orderBy('create_time','desc')->take(3)->get();
        

        return view('web.pages.blog_detail',[
            'slug' => $slug,
            'language_slug' => $language_slug,
            'page' => $category,
            'lastBlog' => $lastBlog
        ]);
    }
}
