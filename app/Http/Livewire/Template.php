<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\SpecialBlock;

class Template extends Component
{
    public $selected;
    public $blockid;

    public function mount($blockid)
    {
        $this->selected = $blockid;
    }

    public function render()
    {
        $kategoriler = SpecialBlock::all();
        return view('livewire.template',[
            'kategoriler' => $kategoriler
        ]);
    }

    public function updatedSelected()
    {
        $this->emit('templateChanged', $this->selected);
    }

}
