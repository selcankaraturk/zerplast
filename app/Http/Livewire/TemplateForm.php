<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Category;
use App\Models\Form;

class TemplateForm extends Component
{
    public $template = 'default'; // Default Blade
    protected $listeners = ['templateChanged' => 'changeForm'];
    public $categoryid;

    public function render()
    {
        $lang = session('lang_slug');
        $category = Category::with(['category_language' => function($q) use($lang){
            $q->where('language_slug',$lang);
        }])
        ->find($this->categoryid) ?? null;
        $forms = Form::all();
        return view('livewire.template-form',[
            'extra' => $category->category_language->extra ?? null,
            'category' => $category,
            'forms' => $forms
        ]);
    }

    public function changeForm($template)
    {
        $this->template = strval($template);
    }
}
