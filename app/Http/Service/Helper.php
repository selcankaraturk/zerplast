<?php

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\Category;
use App\Models\Taxonomy;
use App\Models\CategoryLanguage;
use App\Models\Setting;

function jsonDecoder($source,$key)
{
  if (isset(json_decode($source,true)[$key])) {
    return json_decode($source,true)[$key];
  }else {
    return null;
  }
}

function TextToJson($file){
    $file_name = explode('.',$file)[0];
    return asset('misc/json/'.$file_name.'.json');
}

function Settings($lang){
    return DB::table('settings')
    ->where('language_id',$lang)
    ->first();
}

function jsonDecoderById($id,$lang,$key = 0)
{
  $source = DB::table("category_language")
  ->where("category_id",$id)
  ->where("language_slug",$lang)
  ->first()
  ->special_fields;
  if (isset(json_decode($source,true)[$key])) {
    return json_decode($source,true)[$key];
  }else {
    return null;
  }
}

function SpecialBlocks($block_id)
{
  return DB::table('special_blocks')->where('id',$block_id)->first();
}

function getBlockId($block_id, $language_slug){
    $importantProducts = Category::with([
        'category_language' => function($q) use($language_slug){
            $q->where('language_slug',$language_slug);
        }
    ])->where(['block_id'=> $block_id, 'home_page'=> 1])->get();

    return $importantProducts;
}

function Tarih($timestamp)
{
  setlocale(LC_TIME, 'tr_TR.UTF-8');
  date_default_timezone_set('Europe/Istanbul');
  $aylar = array(
      '0',
      'Ocak',
      'Şubat',
      'Mart',
      'Nisan',
      'Mayıs',
      'Haziran',
      'Temmuz',
      'Ağustos',
      'Eylül',
      'Ekim',
      'Kasım',
      'Aralık'
  );

  $dayofweek = date('w',$timestamp);
  $ay = date('n',$timestamp);
  $trAy = $aylar[$ay];
  return date('j',$timestamp)." ".$trAy." ".date('Y',$timestamp);
  //return date('d.m.Y',$timestamp);
}
function Ay($timestamp){
    setlocale(LC_TIME, 'tr_TR.UTF-8');
    date_default_timezone_set('Europe/Istanbul');
    $aylar = array(
        '0',
        'Ocak',
        'Şubat',
        'Mart',
        'Nisan',
        'Mayıs',
        'Haziran',
        'Temmuz',
        'Ağustos',
        'Eylül',
        'Ekim',
        'Kasım',
        'Aralık'
    );
    $ay = date('n',$timestamp);
    $trAy = $aylar[$ay];
    return $trAy;
}

function getDefaultLanguage()
{
  $settings = DB::table('settings')->where('default_lang',1)->first();
  return $settings->language_id;
}

function getCategoryByParent($parent_id,$lang = null){
    $lang = $lang ?? session('language_slug');
    return Category::with([
            'category_language' => function($q) use($lang){
                $q->where('language_slug',$lang);
            }
        ])
        ->where('parent_id',$parent_id)
        ->orderBy("sorted","asc")
        ->get();
}

function getCategoryById($id,$lang = null){
    $lang = $lang ?? session('language_slug');
    return Category::with([
          'category_language' => function($q) use($lang){
              $q->where('language_slug',$lang);
          }
      ])
      ->where('id',$id)
      ->first();
  }

function getCategoryLanguage($id,$slug)
{
  $settings = DB::table("settings")->where("default_lang",1)->first();
  if (empty($slug)) {
    $slug = $settings->language_id;
  }
  $result = DB::table("category_language")->where("category_id",$id)->where("language_slug",$slug)->first();
  if ($result) {
    return $result;
  }else {
    return null;
  }
}

function getCategoryLanguageByValue($id,$slug,$value)
{
  $settings = DB::table("settings")->where("default_lang",1)->first();
  if (empty($slug)) {
    $slug = $settings->language_id;
  }
  $result = DB::table("category_language")->where("category_id",$id)->where("language_slug",$slug)->first();
  if ($result) {
    return $result->$value;
  }else {
    return null;
  }
}

function menu($lang = null,$parent_id = 1)
{
    $lang = $lang ?? session('language_slug');
    $menuler = Category::with([
        'category_language' => function($q) use($lang){
            $q->where('language_slug',$lang);
        }
    ])
    ->where('parent_id',$parent_id)
    ->orderBy("sorted","asc")
    ->get();
    return $menuler;
}

function taxonomy($category_id,$language_slug = null)
{
    $lang_prefix = "/".$language_slug."/";

    $settings = Setting::where("default_lang",1)->first();

    if ($language_slug == null or $language_slug == $settings->language_id) {
        $language_slug = $settings->language_id;
        $lang_prefix = "/";
    }

    $taxonomy = Taxonomy::where([
        ["category_id","=",$category_id],
        ["language_slug","=",$language_slug]
    ])->first();

    $category = Category::find($category_id);
    //$category_language = CategoryLanguage::where('category_id',$category_id)->where('language_slug',$language_slug)->first();
    if($taxonomy){
        if ($category->url) {
            if(is_numeric($category->url)){
                $taxonomy = Taxonomy::where([
                    ["category_id","=",$category->url],
                    ["language_slug","=",$language_slug]
                ])->first();
            }else{
                return $category->url;
            }
        }
        $count =  $taxonomy->count ? "-".$taxonomy->count : "";
        return $lang_prefix."".$taxonomy->slug."".$count;
    }
    return null;
}

function getLanguage($key,$slug)
{
  $settings = DB::table('settings')->where('default_lang',1)->first();
  if (empty($slug)) {
    $slug = $settings->language_id;
  }
  $lang_value = DB::table("language_value")->where([
    ["language_slug","=",$slug],
    ["language_key","=",$key]
  ]);
  if ($lang_value->exists()) {
    return $lang_value->first()->language_value;
  }else {
    return $key;
  }
}

function getExtensionOf($filename)
{
  $arr = explode(".",$filename);
  return end($arr);
}
