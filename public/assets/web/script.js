const links=document.querySelectorAll('.side-bar li');

$(document).ready(function (){

$('.menu-button').click(function (){
    $('.side-bar').toggleClass("active");
    $('.menu-button').toggleClass("close");
    links.forEach((link,index)=>{
    if(link.style.animation){
        link.style.animation='';
    }else{
        link.style.animation=`linksFade 0.4s ease forwards ${index / 5 + 0.5}s`;
    }
    });
});

    /*$(window).scroll(function () {
        if ($(window).scrollTop() > 0){
            $('nav').css("background-color","#ccc")
        }else {
            $('nav').css("background-color","#131a36")
        }
    });*/
})