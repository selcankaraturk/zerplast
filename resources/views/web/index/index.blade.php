@extends('layouts.web_base')
@section('content')
    @php($intro = getCategoryByParent(7, $language_slug))
    <section class="intro" data-aos="fade-zoom-in"
             data-aos-easing="ease-in-sine"
             data-aos-delay="500" data-aos-offset="0">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-12 h-100">
                    <div class="swiper introSwiper h-100">
                        <div class="swiper-wrapper">
                            @foreach($intro as $item)
                                <div class="swiper-slide">
                                    <img src="{{asset($item->image)}}" class="img-fluid" alt="">
                                </div>
                            @endforeach
                        </div>
                        <div class="swiper-pagination"></div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @php($about = getCategoryById(11, $language_slug))
    <section class="about">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-md-12 back">
                    <div class="row align-items-center">
                        <div class="col-md-6 about-text" data-aos="fade-left"
                             data-aos-duration="300"
                             data-aos-delay="300"
                             data-aos-easing="ease-in-sine">
                            <h1>{{$about->category_language->name}}</h1>

                                {!! $about->category_language->contents !!}

                            <a href="{{taxonomy($about->id, $language_slug)}}">
                                {{getLanguage('devamını oku', $language_slug)}}
                                <i class="fa-solid fa-angle-right"></i>
                            </a>
                        </div>
                        <div class="col-md-6 text-end" data-aos="fade-right"
                             data-aos-duration="300"
                             data-aos-delay="300"
                             data-aos-easing="ease-in-sine">
                            <img src="{{asset($about->image)}}" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @php($product = getCategoryById(12, $language_slug))
    @php($products = getCategoryByParent(12, $language_slug))
    <section class="products" style="background-image: url({{$product->image}})">
        <div class="container position-relative">
            <div class="row justify-content-center">
                <div class="col-12" data-aos="fade-up"
                     data-aos-offset="300"
                     data-aos-easing="ease-in-sine">
                    <h1 class="title">{{$product->category_language->name}}</h1>
                </div>
                <div class="col-10">
                    <div class="row">
                        <div class="swiper productSwiper">
                            <div class="swiper-wrapper">
                                @foreach(getBlockId(6, $language_slug) as $key => $item)
                                    <div class="swiper-slide" data-aos="fade-up"
                                         data-aos-delay="{{400*($key+1)}}"
                                         data-aos-duration="300">
                                        <div class="col-md-12">
                                            <div class="item">
                                                <div class="image">
                                                    <img src="{{asset($item->image)}}"
                                                         class="img-fluid" alt="">
                                                </div>
                                                <div class="text-center">
                                                    <h2>{{$item->category_language->name}}</h2>
                                                    <a href="{{taxonomy($item->id, $language_slug)}}">{{getLanguage('ürün detay', $language_slug)}}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @php($brand = getCategoryById(17, $language_slug))
    @php($brands = getCategoryByParent(17, $language_slug))
    <section class="markalarimiz" style="background-image: url({{asset($brand->image)}})">
        <h2 class="title" data-aos="fade-up"
            data-aos-delay="400"
            data-aos-duration="300">{{$brand->category_language->name}}</h2>
        <div class="marka">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-5"></div>
                    <div class="col-11 col-sm-7 white-area">
                        <div class="row g-5">
                            @foreach($brands as $key => $item)
                            <div class="col-md-5 item text-center" data-aos="fade-up"
                                 data-aos-delay="{{400*($key+1)}}"
                                 data-aos-duration="300">
                                <img src="{{asset($item->image)}}" class="img-fluid" alt="">
                                <p class="brand-contents">
                                    {!! strip_tags($item->category_language->contents) !!}
                                </p>
                            </div>
                            @endforeach
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @php($catalog = getCategoryById(20, $language_slug))
    <section class="catalog">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 white">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 back" style="background-image: url({{asset($catalog->image2)}})">
                                <h2 class="mt-2" data-aos="fade-up"
                                    data-aos-delay="400"
                                    data-aos-duration="300">{{$catalog->category_language->name}}</h2>
                                <div class="row front h-100">
                                    <div class="col-8 col-xl-7 shape">
                                        <!-- <img src="/image/catalog.png" class="img-fluid" alt="">-->
                                    </div>
                                    <div class="col-sm-4 right-side">
                                        <div class="margin">
                                            <p data-aos="fade-up"
                                               data-aos-delay="400"
                                               data-aos-duration="300">{!! $catalog->category_language->description !!}</p>
                                            <div class="button-cover">
                                                <a target="_blank" data-aos="fade-up"
                                                   data-aos-delay="500"
                                                   data-aos-duration="300"
                                                   href="{{$catalog->image}}">{{getLanguage('incele', $language_slug)}}</a>
                                                @php($explode = explode('/',$catalog->image))
                                                @php($explode = end($explode))

                                                <a data-aos="fade-up"
                                                   data-aos-delay="600"
                                                   data-aos-duration="300" href="{{route('download',$explode)}}">{{getLanguage('indir', $language_slug)}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
