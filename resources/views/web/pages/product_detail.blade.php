@extends('layouts.web_base')
@section('content')
    @php($parent = getCategoryById($page->parent_id, $language_slug))
    <section class="breadcrumb" style="background-image: url({{asset($parent->image)}})">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @php($urun = getCategoryById(4, $language_slug)->category_language->name)
                    <div class="bread"> {{getLanguage('ana sayfa', $language_slug)}}
                        <i class="fa-solid fa-angles-right"></i>
                        {{$urun}}

                        <i class="fa-solid fa-angles-right"></i>
                        {{strip_tags(getCategoryById($parent->parent_id, $language_slug)->category_language->name)}}
                        <i class="fa-solid fa-angles-right"></i>
                        {{$parent->category_language->name}}
                        <i class="fa-solid fa-angles-right"></i> {{$page->category_language->name}}
                    </div>
                    <div class="crumb">{{$urun}}</div>
                </div>
            </div>
        </div>
    </section>

    <section class="products-page-detail">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 position-relative" data-aos="fade-right"
                     data-aos-delay="300"
                     data-aos-duration="200">
                    <div class="swiper productDetailSwiper h-100">
                        <div class="swiper-wrapper">
                            @if($page->image)
                                <div class="swiper-slide">
                                    <div class="col-md-12 h-100">
                                        <div class="item">
                                            <img src="{{asset($page->image)}}" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if($page->image2)
                                <div class="swiper-slide">
                                    <div class="col-md-12 h-100">
                                        <div class="item">
                                            <img src="{{asset($page->image2)}}" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if($page->image3)
                                <div class="swiper-slide">
                                    <div class="col-md-12 h-100">
                                        <div class="item">
                                            <img src="{{asset($page->image3)}}" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="swiper-pagination"></div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
                <div class="col-lg-6 mt-5 mt-lg-0" data-aos="fade-left"
                     data-aos-delay="300"
                     data-aos-duration="200">
                    <div class="features">
                        <h3 class="feature-title">
                            {{$page->category_language->description}}
                        </h3>

                        @php($features = json_decode($page->category_language->special_fields))
                        <div class="row mt-4">
                            <div class="col-6 mb-4">
                                <div class="features-item">
                                    <img src="{{asset('assets/web/image/icon-1.png')}}" class="img-fluid" alt="">
                                    <div class="features-text">
                                        @if($features[0])
                                            @php($code = explode(':', $features[0]))
                                        @endif
                                        <p class="text-one">{{$code[0] ?? ''}}</p>
                                        <p class="text-two">{{$code[1] ?? ''}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-4">
                                <div class="features-item">
                                    <img src="{{asset('assets/web/image/icon-2.png')}}" class="img-fluid" alt="">
                                    <div class="features-text">
                                        @if(isset($features[1]))
                                            @php($barcode = explode(':', $features[1]))
                                        @endif

                                        <p class="text-one">{{$barcode[0] ?? ''}}</p>
                                        <p class="text-two">{{$barcode[1] ?? ''}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-4">
                                <div class="features-item">
                                    <img src="{{asset('assets/web/image/icon-3.png')}}" class="img-fluid" alt="">
                                    <div class="features-text">
                                        @if(isset($features[2]))
                                            @php($fix = explode(':', $features[2]))
                                        @endif
                                        <p class="text-one">{{$fix[0] ?? ''}}</p>
                                        <p class="text-two">{{$fix[1] ?? ''}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-4">
                                <div class="features-item">
                                    <img src="{{asset('assets/web/image/icon-4.png')}}" class="img-fluid" alt="">
                                    <div class="features-text">
                                        @if(isset($features[3]))
                                            @php($vol = explode(':', $features[3]))
                                        @endif
                                        <p class="text-one">{{$vol[0] ?? ''}}</p>
                                        <p class="text-two">{{$vol[1] ?? ''}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-4">
                                <div class="features-item">
                                    <img src="{{asset('assets/web/image/icon-5.png')}}" class="img-fluid" alt="">
                                    <div class="features-text">
                                        @if(isset($features[4]))
                                            @php($count = explode(':', $features[4]))
                                        @endif
                                        <p class="text-one">{{$count[0] ?? ''}}</p>
                                        <p class="text-two">{{$count[1] ?? ''}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 mb-4">
                                <div class="features-item">
                                    <img src="{{asset('assets/web/image/icon-6.png')}}" class="img-fluid" alt="">
                                    <div class="features-text">
                                        @if(isset($features[5]))
                                            @php($colour = explode(':', $features[5]))
                                        @endif
                                        <p class="text-one">{{$colour[0] ?? ''}}</p>
                                        <p class="text-two">{{$colour[1] ?? ''}}</p>
                                    </div>
                                </div>
                            </div>
                            <a target="_blank"
                               href="{{$page->category_language->image}}">{{getLanguage('e-katalog', $language_slug)}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="similar-products">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3>{{getLanguage('benzer urunler', $language_slug)}}</h3>
                    <div class="row">
                        <div class="col-12 position-relative">
                            <div class="row productSimilar">
                                <div class="swiper productSimilarSwiper">
                                    <div class="swiper-wrapper">

                                        @foreach($smilarProducts as $key => $item)
                                            <div class="swiper-slide col-3" data-aos="fade-right"
                                                 data-aos-delay="{{200*($key+1)}}"
                                                 data-aos-duration="200">
                                                <div class="item">
                                                    <div class="item-img">
                                                        <img src="{{asset($item->image)}}" class="img-fluid" alt="">
                                                    </div>
                                                    <p>{{$item->category_language->name}}</p>
                                                    <a href="">{{getLanguage('urun detay', $language_slug)}}</a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script>
        var swiper = new Swiper(".productDetailSwiper", {
            loop: true,

            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });
        var swiperSimilar = new Swiper(".productSimilarSwiper", {
            loop: true,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            breakpoints: {
                640: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 30,
                },
                1024: {
                    slidesPerView: 4,
                    spaceBetween: 30,
                },
            }
        });
    </script>
@endsection
