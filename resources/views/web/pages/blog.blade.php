@extends('layouts.web_base')
@section('content')
    <section class="breadcrumb" style="background-image: url({{asset($page->image)}}">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="bread"> {{getLanguage('ana sayfa', $language_slug)}}
                        <i class="fa-solid fa-angles-right"></i>
                        {{$page->category_language->name}}
                    </div>
                    <div class="crumb">
                        {{getLanguage('plastik hakkında', $language_slug)}}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="blog">
        <div class="container">
            <div class="row g-3">
                <div class="page-title">
                    <div class="small-title">{{getLanguage('son başlıklar', $language_slug)}}</div>
                    <div class="title">{{getLanguage('plastik hakkında', $language_slug)}}</div>
                </div>
                @php($children = getCategoryByParent($page->id, $language_slug))
                @foreach($children as $key => $item)
                <div class="col-md-4" data-aos="fade-up"
                     data-aos-delay="{{300*($key+1)}}"
                     data-aos-duration="200">
                    <div class="item">
                        <div class="date">
                            <div class="number">{{date('d', $item->create_time)}}</div>
                            <div class="name">{{Ay($item->create_time)}}</div>
                        </div>
                        <img src="{{$item->image}}" alt="">
                        <div class="item-text">
                            <div class="row comment">
                                @php($by = json_decode($item->category_language->special_fields))
                                <div class="col-6">
                                    <i class="fa-regular fa-user"></i>
                                    <span>{{$by[0] ?? ''}}</span>
                                </div>
                                <div class="col-6">
                                    <i class="fa-regular fa-comments"></i>
                                    <span>{{$by[1] ?? ''}}</span>
                                </div>
                            </div>
                            <h3>
                                {{$item->category_language->name}}
                            </h3>
                            <p>
                                {{ $item->category_language->description }}
                            </p>

                            <a href="{{taxonomy($item->id, $language_slug)}}">
                                {{getLanguage('devamını oku', $language_slug)}}
                                <i class="fa-solid fa-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
