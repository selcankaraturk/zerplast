@extends('layouts.web_base')
@section('content')
    <section class="breadcrumb" style="background-image: url({{asset($page->image)}})">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="bread"> {{getLanguage('ana sayfa', $language_slug)}} <i
                            class="fa-solid fa-angles-right"></i> {{$page->category_language->name}}</div>
                    <div class="crumb">{{$page->category_language->description}}</div>
                </div>
            </div>
        </div>
    </section>

    <section class="vision" style="background-image: url({{asset($page->image2)}})">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="text-cover" data-aos="fade-up"
                         data-aos-delay="300"
                         data-aos-duration="200">
                        <h1>{{$page->category_language->description}}</h1>
                        {!! $page->category_language->contents !!}
                    </div>
                    @php($about = getCategoryByParent($page->id, $language_slug))
                    @php($mission = $about->first())
                    <div class="row">
                        <div class="col-md-10">
                            <div class="text-cover" data-aos="fade-up"
                                 data-aos-delay="400"
                                 data-aos-duration="200">
                                <div class="d-flex align-items-center">
                                    <img src="{{asset($mission->image)}}" alt="">
                                    <h1 class="extra">{{ $mission->category_language->name }}</h1>
                                </div>
                                {!! $mission->category_language->contents  !!}
                            </div>
                            @php($vission = $about->last())
                            <div class="text-cover" data-aos="fade-up"
                                 data-aos-delay="500"
                                 data-aos-duration="200">
                                <div class="d-flex align-items-center">
                                    <img src="{{asset($vission->image)}}" alt="">
                                    <h1 class="extra">{{$vission->category_language->name}}</h1>
                                </div>
                                {!! $vission->category_language->contents  !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">

                </div>
            </div>
        </div>
    </section>
@endsection
