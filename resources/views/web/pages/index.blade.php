@extends('layouts.web_base')
@section('content')
    <section class="breadcrumb" style="background-image: url({{asset($page->image)}})">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="bread"> {{getLanguage('ana sayfa', $language_slug)}} <i
                            class="fa-solid fa-angles-right"></i> {{$page->category_language->name}}</div>
                   
                </div>
            </div>
        </div>
    </section>

    <div class="container mt-4">
      <div class="row">
        <div class="col-12">
          {!! $page->category_language->contents !!}
        </div>
      </div>
    </div>
@endsection
