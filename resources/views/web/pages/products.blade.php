@extends('layouts.web_base')
@section('content')
    @php($urun = getCategoryById(4, $language_slug))
    @php($products = getCategoryByParent(4, $language_slug))
    <section class="breadcrumb" style="background-image: url({{asset($urun->image)}})">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="bread">
                        {{getLanguage('ana sayfa', $language_slug)}}
                        <i class="fa-solid fa-angles-right"></i> {{$urun->category_language->name}}
                        <i class="fa-solid fa-angles-right"></i>
                        @if($page->id == 4)
                            {{$products->first()->category_language->name}}
                        @else
                            {{ strip_tags($page->category_language->name) }}
                        @endif
                    </div>
                    <div class="crumb"> {{$urun->category_language->name}} </div>
                </div>
            </div>
        </div>
    </section>

    <section class="products-page">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        @foreach($products as $key => $item)
                            <li class="nav-item" role="presentation" data-aos="fade-down"
                                data-aos-delay="{{($key+1)*200}}"
                                data-aos-duration="200">
                                <a href="{{taxonomy($item->id, $language_slug)}}" class="nav-link
                        {{($page->id == 4 && $key == 0) || taxonomy($item->id, $language_slug) == '/'.\Str::slug($page->category_language->name) ? 'active':''}}">
                                    <img src="{{asset($item->image)}}" class="img-fluid" alt="">
                                </a>
                                <p class="mt-2">{!! $item->category_language->name  !!}</p>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content mt-5" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                                <div class="col-md-4 mb-md-0 mb-4">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="d-flex align-items-start">
                                                @if($page->id == 4)
                                                    @php($subId = $products->first()->id)
                                                @else
                                                    @php($subId = $page->id)
                                                @endif

                                                @php($subProducts = getCategoryByParent($subId, $language_slug))
                                                <div class="nav flex-column nav-pills w-100 left-menu" id="v-pills-tab"
                                                     role="tablist"
                                                     aria-orientation="vertical" data-aos="fade-right"
                                                     data-aos-delay="500"
                                                     data-aos-duration="200">
                                                    <h3> {{getLanguage('butun', $language_slug)}}
                                                        <span>
                                                            ‘‘{{strip_tags(getCategoryById($subId, $language_slug)->category_language->name)}}’’
                                                        </span>
                                                    </h3>
                                                    @foreach($subProducts as $key => $item)
                                                        <button class="nav-link {{$key == 0? 'active':''}} px-0"
                                                                id="v-pills-home-tab"
                                                                data-bs-toggle="pill" data-bs-target="#v-pills-{{$key}}"
                                                                type="button" role="tab"
                                                                aria-controls="v-pills-{{$key}}"
                                                                aria-selected="true">
                                                            <div class="sub-menu">
                                                                <span>{{$item->category_language->name}}</span>
                                                                <span><i
                                                                        class="fa-solid fa-arrow-right-long"></i></span>
                                                            </div>
                                                        </button>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="tab-content" id="v-pills-tabContent">
                                        @foreach($subProducts as $key => $item)
                                            <div class="tab-pane fade show {{$key == 0 ? 'active': ''}}" id="v-pills-{{$key}}" role="tabpanel"
                                                 aria-labelledby="v-pills-home-tab">
                                                <div class="row g-5">
                                                    @php($subItem = getCategoryByParent($item->id, $language_slug))
                                                    @foreach($subItem as $item2)
                                                        <div class="col-sm-6" data-aos="fade-up"
                                                             data-aos-delay="{{300*($key+1)}}"
                                                             data-aos-duration="200">
                                                            <div class="d-flex">
                                                                <div class="item">
                                                                    <img src="{{$item2->image}}"
                                                                         class="img-fluid" alt="">
                                                                </div>
                                                                <p>{{$item2->category_language->name}}</p>
                                                                <a href="{{taxonomy($item2->id, $language_slug)}}">
                                                                    {{getLanguage('urun detay', $language_slug)}}
                                                                </a>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
