@extends('layouts.web_base')
@section('content')
@php($parent = getCategoryById($page->parent_id, $language_slug) )
    <section class="breadcrumb" style="background-image: url({{asset($parent->image)}})">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="bread"> {{getLanguage('ana sayfa', $language_slug)}}
                        <i class="fa-solid fa-angles-right"></i>
                            {{$parent->category_language->name}}
                        <i class="fa-solid fa-angles-right"></i>
                        {{$page->category_language->name}}
                    </div>
                    <div class="crumb">
                        {{$page->category_language->name}}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="blog-detail">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 mb-4 mb-lg-0">
                    <div class="item">
                        <img src="{{$page->image2}}" class="img-fluid" alt="">
                        <div class="text">
                            <h1>{{ $page->category_language->name }}</h1>
                           {!! $page->category_language->contents !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 ms-auto">
                    <div class="last-titles mb-4">
                        <p class="title"> {{getLanguage('son başlıklar', $language_slug)}} </p>
                        @foreach($lastBlog as $key => $item)
                        <div class="post {{$key == 2 ? '':'mb-4'}}">
                            <img src="{{$item->image}}" alt="">
                            <span>
                            {{$item->category_language->name}}
                        </span>
                        </div>
                        @endforeach
                    </div>
                    <!-- <div class="last-titles category">
                        <p class="title"> Kategoriler </p>
                        <div>
                            PLASTİK <span>( 00 )</span>
                        </div>
                        <div>
                            Plastik Enjeksiyon <span>( 00 )</span>
                        </div>
                        <div>
                            Plastik Hammadde <span>( 00 )</span>
                        </div>
                        <div>
                            Plastik Üretim <span>( 00 )</span>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
@endsection
