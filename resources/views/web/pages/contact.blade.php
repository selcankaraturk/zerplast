@extends('layouts.web_base')
@section('content')
@php($parent = getCategoryById($page->parent_id, $language_slug) )

<section class="breadcrumb" style="background-image: url({{asset($page->image)}})">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="bread"> {{getLanguage('ana sayfa', $language_slug)}}
                    <i class="fa-solid fa-angles-right"></i>
                {{$page->category_language->name}}
                </div>
                <div class="crumb">
                    {{$page->category_language->description}}
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-5" data-aos="fade-right"
                 data-aos-delay="300"
                 data-aos-duration="200">
                <h1 class="contact-title">
                {{$page->category_language->description}}
                </h1>
                <form action="">
                    <div class="mb-3">
                        <input class="form-control" type="text" placeholder="{{getLanguage('ad soyad', $language_slug)}}">
                    </div>
                    <div class="mb-3 row g-3">
                        <div class="col-6">
                            <input class="form-control" type="text" placeholder="{{getLanguage('telefon', $language_slug)}}">
                        </div>
                        <div class="col-6">
                            <input class="form-control" type="text" placeholder="{{getLanguage('e-posta', $language_slug)}}">
                        </div>
                    </div>
                    <div class="mb-3">
                        <input class="form-control" type="text" placeholder="{{getLanguage('konu', $language_slug)}}">
                    </div>
                    <div class="mb-3">
                        <textarea class="form-control" name="" cols="30" rows="10" placeholder="{{getLanguage('mesaj', $language_slug)}}"></textarea>
                    </div>
                    <div class="mt-4">
                        <button>{{getLanguage('Gönder', $language_slug)}}</button>
                    </div>

                </form>
            </div>
            <div class="col-md-5 information" data-aos="fade-left"
                 data-aos-delay="300"
                 data-aos-duration="200">
                <div class="mb-3">
                    @php($info = json_decode($page->category_language->special_fields))
                    <h3>{{getLanguage('adres', $language_slug)}}</h3>
                    <div class="d-flex align-items-start">
                        <img src="{{asset('assets/web/image/map.png')}}" alt="">
                        <p>
                            {{$info[0] ?? ''}}
                        </p>
                    </div>
                </div>
                <div class="mb-4">
                    <h3>{{getLanguage('telefon', $language_slug)}}</h3>
                    <div class="d-flex align-items-start">
                        <img src="{{asset('assets/web/image/b-tel.png')}}" alt="">
                        <p>
                            {{$info[1] ?? ''}}
                        </p>
                    </div>
                </div>
                <div class="mb-3">
                    <h3>{{getLanguage('e-posta', $language_slug)}}</h3>
                    <div class="d-flex align-items-start">
                        <img src="{{asset('assets/web/image/b-mail.png')}}" alt="">
                        <p>
                            {{$info[2] ?? ''}}
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-12" data-aos="fade-up"
                 data-aos-delay="300"
                 data-aos-duration="200">
                {!! $info[3] ?? '' !!}
            </div>
        </div>
    </div>
</section>

@endsection
