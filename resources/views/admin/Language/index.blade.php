@extends('layouts.admin-base')

@section('yeni_ekle')
<a href="{{ route('language_create') }}" class="btn btn-success" type="button">Yeni Ekle</a>
@endsection

@section('javascripts')
<script>
    $(function(){
        $(".init-lang").on('click',function(){
            var btn = $(this);
            var btnText = btn.text();
            btn.prop("disabled",true).html('<div class="spinner-border spinner-border-sm text-primary" role="status"></div>');
            let url = $(this).data("url");
            $.get(url).done(function(res){
                let popText = '';
                if(res.status === true && res.isNew === false){
                    popText = "Dil Zaten Senkronize Durumda !";
                }else if(res.status === true && res.isNew === true){
                    popText = 'Yeni Dil Başarıyla Senkronize Edildi!'
                }else{
                    popText = 'Something Went Wrong!';
                }
                btn.prop("disabled",false).html(btnText).css("background-color","#2eb85c").attr('data-content',popText).popover('show');
                setTimeout(() => {
                    btn.popover('hide');
                }, 2000);
            }).fail(function(err){
                console.error(err);
            });
            return false;
        })
        $("a#deleteLang").click(function(){
            let conf = confirm("Bu dili tüm içerikleriyle birlikte silmek ister misiniz?");
            if(conf){
                return true;
            }else{
                let url = $(this).attr("href") + "?onlylang=true";
                conf = confirm("Bu işlemi onayladığınızda; sadece dil silinecek fakat dile ait içerikler veritabanında kalmaya devam edecektir.");
                if(conf){
                    window.location = encodeURI(url);
                    return false;
                }else{
                    return false;
                }
            }
        });
    });
</script>
@endsection

@section('content')


  <table class="table mb-0 table-responsive-sm table-hover table-outline">
  <thead class="thead-light">
  <tr>
  <th>
  Adı <i class="fas fa-language"></i>
  </th>
  <th>Ön Eki</th>
  <th class="text-center">İşlemler</th>
  </tr>
  </thead>
  <tbody>

    @foreach ($languages as $key => $language)
      <tr>
        <td> {{ $language->name }} </td>
        <td> {{ $language->slug }} </td>
        <td class="text-right" width="35%">
        <button
        type="button"
        data-url="{{ route('language_init',([ 'lang' => $language->slug ])) }}"
        class="text-white btn btn-lg btn-warning init-lang"
        data-container="body"
        data-trigger="manual"
        data-toggle="popover"
        data-placement="top"
        style="min-width: 100px"
        data-content="Dil Başarıyla Senkronize Edildi">
            Senkronize et
        </button>
        <a href="{{ route('language_edit',([ 'id' => $language->id ])) }}" class="btn btn-lg btn-primary" type="button">Düzenle</a>
        <a href="{{ route('language_delete',([ 'id' => $language->id ])) }}" class="btn btn-lg btn-danger" id="deleteLang" type="button">Sil</a>
        </td>
      </tr>
    @endforeach



  </tbody>
  </table>

<small class="mt-3 w-100 d-block">
    @if($lostContents)
        Veritabanında,
        @foreach ($lostContents as $item)
            <strong>{{ $item["name"] }} {{ !$loop->last ? ',' : null }}</strong>
        @endforeach
        diline/dillerine Ait Verilerin kayıp olduğu görünüyor. lütfen dili senkronize ediniz.
        <br>
    @endif
</small>
@endsection
