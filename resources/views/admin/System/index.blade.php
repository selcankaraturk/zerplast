@extends('layouts.admin-base')

@section('yeni_ekle')

@endsection

@section('javascripts')
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script>
    $(function() {
        //$('#delete-cache').bootstrapToggle('off');
        $('#delete-cache').change(function(val){
            let status = $(this).prop('checked'); // true or false
            if(status === true){
                $.get('{{ route("system.clearcache") }}').done(function (data){
                    if(data.status == 'ok'){
                        console.log("cache cleared");
                    }else{
                        console.log("Something went wrong!");
                        $('#delete-cache').bootstrapToggle('off');
                    }
                });
            }
        });

        //Maintenance Mode

        $('#maintenance-mode').change(function(val){
            let status = $(this).prop('checked'); // true or false
            if(status === true){
                $.get('{{ route("system.down") }}').done(function (data){
                    if(data.status == 'ok'){
                        console.log("Maintenance mode aktif");
                    }else{
                        console.log("Something went wrong!");
                    }
                });
            }else if(status === false){
                $.get('{{ route("system.up") }}').done(function (data){
                    if(data.status == 'ok'){
                        console.log("Maintenance mode Pasif");
                    }else{
                        console.log("Something went wrong!");
                    }
                });
            }
        });
    });
</script>
@endsection

@section('head')
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
@endsection

@section('content')

<div class="table-like-list">
    <div class="row">
        <div class="col col-lg-5">
            Cache Sil (Laravel)
        </div>
        <div class="col col-lg-7">
            <input type="checkbox" id="delete-cache" data-on="Silindi" data-off="Sil" data-toggle="toggle" data-size="lg">
        </div>
    </div>

    <div class="row">
        <div class="col col-lg-5">
            Bakım Modu (Maintenance Mode)<br>
            <small>Tüm sistemi geçici olarak kapatır</small>
        </div>
        <div class="col col-lg-7">
            <input type="checkbox" {{ $maintenance ? 'checked' : null }} id="maintenance-mode" data-on="Açık" data-off="Kapalı" data-toggle="toggle" data-size="lg">
        </div>
    </div>

    <div class="row">
        <div class="col col-lg-5">
            Migration<br>
            <small>Artisan Migrate</small>
        </div>
        <div class="col col-lg-7">
            <a href="{{ route('system.migrate') }}" type="button" class="btn btn-secondary ml-3 btn-pop">
                Migrate
            </a>
        </div>
    </div>
</div>

@endsection
