@extends('layouts.admin-base')
@section('javascripts')
    @livewireScripts
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script src="{{ asset('assets/admin/ckeditor/ckeditor.js') }}"></script>
    <script>
        function initializeEditor(){
            setTimeout(() => {
                if(document.querySelectorAll('.ckeditor').length > 0){
                document.querySelectorAll('.ckeditor').forEach(e => {
                    let editorid = e.id
                    if(CKEDITOR.instances[editorid]) CKEDITOR.instances[editorid].destroy();
                    CKEDITOR.replace(editorid,{
                        height: 250,
                        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{ csrf_token() }}',
                        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{ csrf_token() }}'
                    });
                });
            }
            }, 100);
        }

        Livewire.hook('message.processed', () => {
            $('.lfm').filemanager('image');
            initializeEditor();
        });

        $(function(){
            $('.lfm').filemanager('image');
            initializeEditor();

            $("#addOzel").click(function(){
            var cloned = $(".ozel_alan").eq(0).clone();
            cloned.find("label").attr("for","ozel_alan-"+$(".ozel_alan").length);
            cloned.find("input").attr("value","").val("").last().attr("id","ozel_alan-"+$(".ozel_alan").length).attr("name","ozelalan[]");
            cloned.append('<div onclick="remove_special(this);" class="remove_special"> <i class="fas fa-trash-alt"></i> </div>');
            cloned.insertAfter(".ozel_alan:last");
            });
        });

        function remove_special(ths){
            $(ths).parent().remove();
        }
    </script>
@endsection
@section('head')
    @livewireStyles
@endsection

@section('yeni_ekle')
<strong> <a href="{{ route('CMS Yönetimi',['id'=>$category->parent_id]) }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Geri</a></strong>
@endsection

@section('ContentHeader')
  @if ($category->parent_id > 0)
    <div><a href="{{ route('CMS Yönetimi',['id'=>$category->parent_id]) }}" style="white-space: nowrap;">{{ getCategoryLanguage($category->parent_id,session('lang_slug'))->name }}</a></div>
    <div class="mx-2"><i class="fas fa-long-arrow-alt-right"></i></div>
  @endif
    <div><strong style="white-space: nowrap;">{{ $category->category_language->name }}</strong></div>
@endsection

@section('content')
  <div class="card">
  <form class="" action="" method="post" enctype="multipart/form-data">
  @csrf
  <div class="card-header">
    <div class="row">
      <div class="text-left col-12 col-md-6 d-flex align-items-center">
        <strong class="my-auto mr-2 d-block">{{ $category->category_language->name }}</strong> -
        <strong class="my-auto ml-2 d-block">{{ Route::currentRouteName() }}</strong>
      </div>
      <div class="text-right col-12 col-md-6">

      </div>
    </div>

  </div>
  <div class="card-body">
    <div class="row">

      <div class="col-12 col-md-6">
        <div class="form-group">
        <label for="input1">Başlık</label>
        <input class="form-control" id="input1" name="name" type="text" value="{{ $category->category_language->name }}" placeholder="Başlık" required>
        </div>
      </div>

      <div class="col-12 col-md-6">
        <div class="form-group">
        <label for="input2">Sıra</label>
        <input class="form-control" id="input2" name="sorted" type="number" placeholder="Sıra" value="{{ $category->sorted }}" required>
        </div>
      </div>

      <div class="col-12 col-md-6">
        <livewire:template :blockid="$category->block_id" />
      </div>

      <div class="col-12 col-md-6">
        <div class="form-group">
        <label for="input3">Harici URL</label>
        <small>( İçerik ID'si girilebilir )</small>
        <input class="form-control" id="input3" name="url" type="text" placeholder="URL veya ID" value="{{ $category->url }}">
        </div>
      </div>

    </div>

  <livewire:template-form :template="$category->block_id" :categoryid="$category->id" />

  <div class="form-group row mt-5">
    @if (!empty($category->category_language->special_fields) and count(json_decode($category->category_language->special_fields,true)) > 0)

      @foreach (json_decode($category->category_language->special_fields,true) as $key => $value)
      <div class="mb-3 col-12 col-md-3 ozel_alan">
          <label for="ozel_alan-{{ $key }}">Özel Alan <i class="fas fa-palette colorp"></i></label>
            <input class="form-control" id="ozel_alan-{{ $key }}" name="ozelalan[]" type="text" placeholder="Özel Alan" value="{{ $value }}">
          @if (!($loop->first))
            <div onclick="remove_special(this);" class="remove_special"> <i class="fas fa-trash-alt"></i> </div>
          @endif
        </div>
      @endforeach

    @else
    <div class="mb-3 col-12 col-md-3 ozel_alan">
        <label for="ozel_alan">Özel Alan <i class="fas fa-palette colorp"></i></label>
        <input class="form-control" id="ozel_alan" name="ozelalan[]" type="text" placeholder="Özel Alan">
    </div>
    @endif

    <div class="mb-3 col-12 col-md-3 d-flex align-items-end">
      <i class="fas fa-plus-square" id="addOzel" style="color:green; font-size:34px; cursor:pointer;"></i>
    </div>
  </div>

  <div class="mt-5 form-group">
    <div class="row">
      <div class="col-12">
        <div class="form-group">
        <h3><strong>Seo Ayarları</strong></h3>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
          <div class="form-group">
          <label for="input11">Seo Link</label>
          <input type="text" class="form-control" id="input11" placeholder="Seo Link" name="seo_link" value="@if ($category->url){{ $category->url }}@else{{ $taxonomy->slug }}@endif" @if ($category->url) disabled @endif>
          </div>
      </div>
      <div class="col-12">
          <div class="form-group">
          <label for="input12">Seo Başlık</label>
          <input type="text" class="form-control" id="input12" placeholder="Seo Başlık" name="seo_title" value="{{ $category->category_language->seo_title }}">
          </div>
      </div>
      <div class="col-12">
          <div class="form-group">
          <label for="input13">Seo Açıklama</label>
          <textarea class="form-control" id="input13" rows="2" placeholder="Seo Açıklama" name="seo_description">{{ $category->category_language->seo_description }}</textarea>
          </div>
      </div>
      <div class="col-12">
          <div class="form-group">
          <label for="input14">Seo Anahtar Kelime</label>
          <input type="text" class="form-control" id="input14" placeholder="Seo Anahtar Kelime" name="seo_keywords" value="{{ $category->category_language->seo_keywords }}">
          </div>
      </div>
    </div>

  </div>


  </div>

  <div class="card-footer">
  <button class="btn btn-md btn-primary" type="submit"> Kaydet</button>
  <button class="btn btn-md btn-danger" type="reset"> Reset</button>
  </div>
</form>
  </div>

@endsection
