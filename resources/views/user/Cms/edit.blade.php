@extends('layouts.user-base')
@section('javascripts')
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script src="{{ asset('assets/admin/ckeditor/ckeditor.js') }}"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script>
        $(function(){
            $('.lfm').filemanager('image');
        });
        CKEDITOR.replace( 'contents',{
        height: 450,
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{ csrf_token() }}',
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{ csrf_token() }}'
        });

    </script>
@endsection
@section('head')

@endsection

@section('yeni_ekle')

@endsection

@section('content')


  <div class="card">
  <form class="" action="" method="post" enctype="multipart/form-data">
  @csrf
  <div class="card-header">
    <div class="row">
      <div class="col-12 col-md-6 text-left d-flex align-items-center">
        <strong class="my-auto d-block">{{ Route::currentRouteName() }}</strong>
      </div>
      <div class="col-12 col-md-6 text-right">
        <strong> <a href="{{ route('user.CMS Yönetimi',['id'=>$category->parent_id]) }}" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Geri</a></strong>
      </div>
    </div>

  </div>
  <div class="card-body">

  <div class="form-group">
  <label for="input1">Başlık</label>
  <input class="form-control" id="input1" name="name" type="text" value="{{ $category_language->name }}" placeholder="Başlık" required>
  </div>

  <div class="form-group">
  <label for="input2">Sıra</label>
  <input class="form-control" id="input2" name="sorted" type="number" placeholder="Sıra" value="{{ $category->sorted }}" required>
  </div>

  <div class="form-group">
  <label for="input3">Harici URL</label>
  <input class="form-control" id="input3" name="url" type="text" placeholder="URL" value="{{ $category->url }}">
  </div>

  <div class="form-group">
  <label for="input4">Kategori Türü</label>
  <select class="form-control" id="input4" name="category" required>
    @foreach ($kategoriler as $key => $value)
      <option value="{{ $value->id }}" @if ($category->block_id == $value->id) selected @endif>{{ $value->name }}</option>
    @endforeach
 </select>
  </div>

  <div class="form-group">
  <div class="d-flex flex-row bd-highlight mb-3">
    <div class="p-2 bd-highlight">
      <div class="ml-1 custom-control custom-switch mb-3">
      <input type="checkbox" class="custom-control-input" name="top_menu" value="1" id="customSwitch1" @if ($category->top_menu == 1) checked @endif>
      <label class="custom-control-label" for="customSwitch1">Üst Menüde Göster?</label>
      </div>
    </div>
    <div class="p-2 bd-highlight">
      <div class="ml-1 custom-control custom-switch mb-3">
      <input type="checkbox" class="custom-control-input" id="customSwitch2" name="home_page" value="1" @if ($category->home_page == 1) checked @endif>
      <label class="custom-control-label" for="customSwitch2">Anasayfada Göster?</label>
      </div>
    </div>
  </div>
  </div>

  <div class="form-group">
  <label for="input7">Durumu</label>
  <select class="custom-select mb-3" id="input7" name="status">
    <option value="1" @if ($category->status == 1) selected @endif>Aktif</option>
    <option value="0" @if ($category->status == 0) selected @endif>Pasif</option>
  </select>
  </div>

  <div class="form-group">
  <label for="input8">Form Seçiniz</label>
  <select class="custom-select mb-3" id="input8" name="form_id">
    <option value="0" selected>-- Form Seç --</option>
    @foreach ($forms as $key => $form)
      <option value="{{ $form->id }}" @if ($category->form_id == $form->id) selected @endif>{{ $form->id }} - {{ $form->name }}</option>
    @endforeach
  </select>
  </div>

  <div class="form-group">
    <label for="input9">Özet</label>
    <textarea class="form-control" id="input9" rows="3" placeholder="Özet" name="description">{!! $category_language->description !!}</textarea>
  </div>

  <div class="form-group">
    <label for="contents">İçerik</label>
    <textarea name="contents" id="contents">{!! $category_language->contents !!}</textarea>
  </div>

  <div class="form-group">
    <div class="row">
      <div class="col-12">
        <label>Tüm Dillerde Ortak Resimler / Dosyalar :</label>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4">

        <div class="row">
          <div class="col-12">
                <div class="input-group">
                    <span class="input-group-btn">
                        <a id="lfm" data-input="thumbnail_c_1" data-preview="holder_c_1" class="lfm btn btn-primary text-light d-flex align-items-center justify-content-center">
                            <i class="fas fa-image mr-2 fa-lg"></i> Seç
                        </a>
                    </span>
                <input id="thumbnail_c_1" class="form-control" type="text" name="category_image" value="{{ $category->image }}">
                </div>
                <div id="holder_c_1" style="margin-top:15px;">
                    <img src="{{ $category->image }}" class="img-fluid" style="max-height: 200px" alt="">
                </div>
          </div>
        </div>

      </div>

      <div class="col-lg-4">

        <div class="row">
          <div class="col-12">
                <div class="input-group">
                <span class="input-group-btn">
                    <a id="lfm" data-input="thumbnail_c_2" data-preview="holder_c_2" class="lfm btn btn-primary text-light d-flex align-items-center justify-content-center">
                        <i class="fas fa-image mr-2 fa-lg"></i> Seç
                    </a>
                </span>
                <input id="thumbnail_c_2" class="form-control" type="text" name="category_image2" value="{{ $category->image2 }}">
                </div>
                <div id="holder_c_2" style="margin-top:15px;">
                    <img src="{{ $category->image2 }}" class="img-fluid" style="max-height: 200px;" alt="">
                </div>
          </div>
        </div>

      </div>

      <div class="col-lg-4">

        <div class="row">
          <div class="col-12">
                <div class="input-group">
                    <span class="input-group-btn">
                        <a id="lfm" data-input="thumbnail_c_3" data-preview="holder_c_3" class="lfm btn btn-primary text-light d-flex align-items-center justify-content-center">
                            <i class="fas fa-image mr-2 fa-lg"></i> Seç
                        </a>
                    </span>
                <input id="thumbnail_c_3" class="form-control" type="text" name="category_image3" value="{{ $category->image3 }}">
                </div>
                <div id="holder_c_3" style="margin-top:15px;">
                    <img src="{{ $category->image3 }}" class="img-fluid" style="max-height: 200px;" alt="">
                </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <div class="form-group">
    <div class="row">
      <div class="col-12">
        <label>{{ session('lang_name') }} Diline Ait Resimler / Dosyalar :</label>
      </div>
    </div>
    <div class="row">

      <div class="col-lg-4">
        <div class="row">
        <div class="col-12">
            <div class="input-group">
                <span class="input-group-btn">
                    <a id="lfm" data-input="thumbnail_cl_1" data-preview="holder_cl_1" class="lfm btn btn-primary text-light d-flex align-items-center justify-content-center">
                        <i class="fas fa-image mr-2 fa-lg"></i> Seç
                    </a>
                </span>
            <input id="thumbnail_cl_1" class="form-control" type="text" name="language_image" value="{{ $category_language->image }}">
            </div>
            <div id="holder_cl_1" style="margin-top:15px;">
                <img src="{{ $category_language->image }}" class="img-fluid" style="max-height: 200px;" alt="">
            </div>
        </div>
        </div>
      </div>

      <div class="col-lg-4">
        <div class="row">
        <div class="col-12">
            <div class="input-group">
                <span class="input-group-btn">
                    <a id="lfm" data-input="thumbnail_cl_2" data-preview="holder_cl_2" class="lfm btn btn-primary text-light d-flex align-items-center justify-content-center">
                        <i class="fas fa-image mr-2 fa-lg"></i> Seç
                    </a>
                </span>
            <input id="thumbnail_cl_2" class="form-control" type="text" name="language_image2" value="{{ $category_language->image2 }}">
            </div>
            <div id="holder_cl_2" style="margin-top:15px;">
                <img src="{{ $category_language->image2 }}" class="img-fluid" style="max-height: 200px;" alt="">
            </div>
        </div>
        </div>
      </div>

      <div class="col-lg-4">
        <div class="row">
        <div class="col-12">
            <div class="input-group">
                <span class="input-group-btn">
                    <a id="lfm" data-input="thumbnail_cl_3" data-preview="holder_cl_3" class="lfm btn btn-primary text-light d-flex align-items-center justify-content-center">
                        <i class="fas fa-image mr-2 fa-lg"></i> Seç
                    </a>
                </span>
            <input id="thumbnail_cl_3" class="form-control" type="text" name="language_image3" value="{{ $category_language->image3 }}">
            </div>
            <div id="holder_cl_3" style="margin-top:15px;">
                <img src="{{ $category_language->image3 }}" class="img-fluid" style="max-height: 200px;" alt="">
            </div>
        </div>
        </div>
      </div>

    </div>

  </div>

  <div class="form-group mt-5">
    <div class="row">
      <div class="col-12">
        <div class="form-group">
        <h3><strong>Seo Ayarları</strong></h3>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
          <div class="form-group">
          <label for="input11">Seo Link</label>
          <input type="text" class="form-control" id="input11" placeholder="Seo Link" name="seo_link" value="@if ($category->url){{ $category->url }}@else{{ $taxonomy->slug }}@endif" @if ($category->url) disabled @endif>
          </div>
      </div>
      <div class="col-12">
          <div class="form-group">
          <label for="input12">Seo Başlık</label>
          <input type="text" class="form-control" id="input12" placeholder="Seo Başlık" name="seo_title" value="{{ $category_language->seo_title }}">
          </div>
      </div>
      <div class="col-12">
          <div class="form-group">
          <label for="input13">Seo Açıklama</label>
          <textarea class="form-control" id="input13" rows="2" placeholder="Seo Açıklama" name="seo_description" value="{{ $category_language->seo_description }}"></textarea>
          </div>
      </div>
      <div class="col-12">
          <div class="form-group">
          <label for="input14">Seo Anahtar Kelime</label>
          <input type="text" class="form-control" id="input14" placeholder="Seo Anahtar Kelime" name="seo_keywords" value="{{ $category_language->seo_keywords }}">
          </div>
      </div>
    </div>

  </div>


  </div>

  <div class="card-footer">
  <button class="btn btn-md btn-primary" type="submit"> Kaydet</button>
  <button class="btn btn-md btn-danger" type="reset"> Reset</button>
  </div>
</form>
  </div>

@endsection
