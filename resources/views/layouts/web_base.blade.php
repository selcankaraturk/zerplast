<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
          integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css"
    />
    <link rel="shortcut icon" href="{{asset('assets/web/image/favicon-32x32.png')}}"/>
    <link rel="stylesheet" href="{{asset('assets/web/style.css')}}">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <title>{{json_decode(Settings($language_slug)->special_fields)->title ?? ''}}</title>
    <style>

    </style>
</head>
<body>
<nav>

    <div class="container">
        <div class="row menu-top">
            <ul class="social">
                <li><a href="{{json_decode(Settings($language_slug)->special_fields)->instagram ?? ''}}"><i
                            class="fa-brands fa-instagram"></i></a></li>
                <li><a href="{{json_decode(Settings($language_slug)->special_fields)->youtube ?? ''}}"><i
                            class="fa-brands fa-youtube"></i></a></li>
                <li><a href="{{json_decode(Settings($language_slug)->special_fields)->facebook ?? ''}}"><i
                            class="fa-brands fa-facebook-f"></i></a></li>
                <li> <a target="_blank" href="{{getCategoryById(110,$language_slug)->image}}">
                        {{getLanguage('e-katalog', $language_slug)}}
                    </a>
                </li>
                <li class="language dropdown">
                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="lang"
                       data-bs-toggle="dropdown" aria-expanded="false">
                        {{$language_slug == 'tr' ? 'TR':'EN'}}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="lang">
                        <li><a class="dropdown-item" href="{{$language_slug == 'tr' ? '/en':'/tr'}}">
                                {{$language_slug == 'tr' ? 'EN':'TR'}}
                            </a></li>
                    </ul>
                </li>
                <li class="search"><a href=""><i class="fa-solid fa-magnifying-glass"></i></a></li>
            </ul>
        </div>
        <div class="row menu-bottom">
            <div class="logo">
                <a href="{{getDefaultLanguage()===$language_slug ? '/': '/'.$language_slug }}">
                    <img src="{{asset('uploads/'.Settings($language_slug)->logo)}}" class="img-fluid" alt="">
                </a>
            </div>
            <ul class="menu-bar">
                @foreach(menu($language_slug) as $key => $item)
                    @if($key == 0)
                        <li><a class="active"
                               href="{{taxonomy($item->id, $language_slug)}}">{{$item->category_language->name}}</a>
                        </li>
                    @elseif($item->id == 4)
                        <li class="dropdown"><a class=""
                                                href="{{taxonomy($item->id, $language_slug)}}"
                            >{{$item->category_language->name}}
                                <i class="fa-solid fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                @php($urun = getCategoryByParent($item->id, $language_slug))
                                @foreach($urun as $item2)
                                    <li><a class="dropdown-item"
                                           href="{{taxonomy($item2->id, $language_slug)}}">{{strip_tags($item2->category_language->name)}}</a>
                                    </li>
                                @endforeach

                            </ul>
                        </li>
                    @else
                        <li>
                            <a class="{(isset($slug) && $slug == \Str::slug($item->category_language->name) ? 'active':''}}"
                               href="{{taxonomy($item->id, $language_slug)}}">{{$item->category_language->name}}</a>
                        </li>
                    @endif
                @endforeach

            </ul>
            <ul class="side-bar">
                @foreach(menu($language_slug) as $key => $item)
                    @if($key == 0)
                        <li><a class="active"
                               href="{{taxonomy($item->id, $language_slug)}}">{{$item->category_language->name}}</a>
                        </li>
                    @else
                        <li>
                            <a class="{(isset($slug) && $slug == \Str::slug($item->category_language->name) ? 'active':''}}"
                               href="{{taxonomy($item->id, $language_slug)}}">{{$item->category_language->name}}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
            <div class="menu-button">
                <div class="line-1"></div>
                <div class="line-2"></div>
                <div class="line-3"></div>
            </div>
        </div>

    </div>
</nav>

<section class="e-catalog">
    <div>
        <a target="_blank" href="{{getCategoryById(110,$language_slug)->image}}">
            {{getLanguage('e-katalog göz atın', $language_slug)}}
        </a>
    </div>
</section>

@yield('content')

<section class="footer">
    <div class="container top">
        <div class="row justify-content-center">
            <div class="logo">
                <img src="{{asset('assets/web/image/footer-logo.png')}}" class="img-fluid" alt="">
            </div>
        </div>
        <div class="row menu">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-sm-6" data-aos="fade-up"
                         data-aos-delay="400"
                         data-aos-duration="200">
                        @php($menu = getCategoryById(1, $language_slug))
                        <h3>{{$menu->category_language->name}}</h3>
                        <ul>
                            @foreach(menu($language_slug) as $key => $item)
                                @if($key == 0)
                                    <li><a class="active"
                                           href="{{taxonomy($item->id, $language_slug)}}">{{$item->category_language->name}}</a>
                                    </li>
                                @else
                                    <li>
                                        <a class="{(isset($slug) && $slug == \Str::slug($item->category_language->name) ? 'active':''}}"
                                           href="{{taxonomy($item->id, $language_slug)}}">{{$item->category_language->name}}</a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-sm-6" data-aos="fade-up"
                         data-aos-delay="500"
                         data-aos-duration="200">
                        @php($urun = getCategoryById(4, $language_slug))
                        <h3>{{$urun->category_language->name}}</h3>
                        <ul>
                            @foreach(menu($language_slug, 4) as $item)
                                <li><a href="{{taxonomy($item->id, $language_slug)}}">
                                        {{strip_tags($item->category_language->name)}}
                                    </a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-10" data-aos="fade-up"
                         data-aos-delay="600"
                         data-aos-duration="200">
                        <h3>{{getLanguage('iletişim', $language_slug)}}</h3>
                        <ul>
                            @php($info = json_decode(getCategoryById(6, $language_slug)->category_language->special_fields))
                            <li>
                                <div class="d-flex align-items-start">
                                    <img src="{{asset('assets/web/image/konum.png')}}" alt="">
                                    <p class="navigate">
                                        {{$info[0] ?? ''}}
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="d-flex align-items-start">
                                    <img src="{{asset('assets/web/image/tel.png')}}" alt="">
                                    <p class="navigate">
                                        {{$info[1] ?? ''}}
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="d-flex align-items-start">
                                    <img src="{{asset('assets/web/image/mail.png')}}" alt="">
                                    <p class="navigate">
                                        {{$info[2] ?? ''}}
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-2" data-aos="fade-up"
                         data-aos-delay="700"
                         data-aos-duration="200">
                        <h3 style="color: #efefef">0</h3>
                        <ul class="social text-end">
                            <li>
                                <a target="_blank"
                                   href="{{json_decode(Settings($language_slug)->special_fields)->instagram ?? ''}}">
                                    <img src="{{asset('assets/web/image/insta.png')}}" alt="">
                                </a>
                            </li>
                            <li>
                                <a target="_blank"
                                   href="{{json_decode(Settings($language_slug)->special_fields)->youtube ?? ''}}">
                                    <img src="{{asset('assets/web/image/youtube.png')}}" alt="">
                                </a>
                            </li>
                            <li>
                                <a target="_blank"
                                   href="{{json_decode(Settings($language_slug)->special_fields)->facebook ?? ''}}">
                                    <img src="{{asset('assets/web/image/face.png')}}" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container bottom">
        <div class="row">
            <div class="col-md-6">
                <p>
                    {{getLanguage('copyright', $language_slug)}}
                </p>
            </div>
            <div class="col-md-6 text-md-end">
                <p>

                    <a href="{{taxonomy(42, $language_slug)}}">{{getCategoryById(42, $language_slug)->category_language->name}}</a>
                    | <a
                        href="{{taxonomy(43, $language_slug)}}">{{getCategoryById(43, $language_slug)->category_language->name}}</a>
                    | <a
                        href="{{taxonomy(44, $language_slug)}}">{{getCategoryById(44, $language_slug)->category_language->name}}</a>
                </p>
            </div>
        </div>
    </div>
</section>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
<script
    src="https://code.jquery.com/jquery-3.6.0.js"
    integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>
<script src="{{asset('assets/web/script.js')}}"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script>
    AOS.init();
    var swiper = new Swiper(".introSwiper", {
        loop: true,
        slidesPerView: "auto",
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });
    var swiper2 = new Swiper(".productSwiper", {
        loop: true,
        slidesPerView: "auto",
        spaceBetween: 10,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        breakpoints: {
            640: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 40,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 100,
            },
            1399: {
                slidesPerView: 3,
                spaceBetween: 100,
            },
        }
    });
</script>
@yield('js')
</body>
</html>
