<div>
    @php
        $view = View::exists('livewire.template-form.form-'.$template) ? 'form-'.$template : 'default';
    @endphp
    @include('livewire.template-form.'.$view)
</div>
