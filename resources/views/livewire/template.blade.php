
<div class="form-group">
<label for="input4">Template</label>
<select class="form-control" id="input4" name="category" wire:model="selected" required>
    <option value="" hidden {{ $blockid ? null : 'selected' }}> -- Seçiniz -- </option>
    @foreach ($kategoriler as $key => $value)
        <option value="{{ $value->id }}" @if ($blockid == $value->id) selected @endif>{{ $value->name }}</option>
    @endforeach
</select>
</div>
