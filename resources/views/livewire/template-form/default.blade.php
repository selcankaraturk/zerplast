<div>
    <div class="row">
        <div class="col-12 col-md-2">
            <div class="form-group">
                <div class="flex-row mb-0 d-flex bd-highlight">
                    <div class="p-2 bd-highlight">
                    <div class="mb-3 ml-1 custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" name="top_menu" value="1" id="customSwitch1" @if ($category && $category->top_menu == 1) checked @endif>
                    <label class="custom-control-label" for="customSwitch1">Üst Menüde Göster?</label>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-2">
            <div class="p-2 bd-highlight">
                <div class="mb-3 ml-1 custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="customSwitch2" name="home_page" value="1" @if ($category && $category->home_page == 1) checked @endif>
                    <label class="custom-control-label" for="customSwitch2">Anasayfada Göster?</label>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="input7">Durumu</label>
        <select class="mb-3 custom-select" id="input7" name="status">
          <option value="1" @if ($category && $category->status == 1) selected @endif>Aktif</option>
          <option value="0" @if ($category && $category->status == 0) selected @endif>Pasif</option>
        </select>
        </div>

        <div class="form-group">
        <label for="input8">Form Seçiniz</label>
        <select class="mb-3 custom-select" id="input8" name="form_id">
          <option value="0" selected>-- Form Seç --</option>
          @foreach ($forms as $key => $form)
            <option value="{{ $form->id }}" @if ($category && $category->form_id == $form->id) selected @endif>{{ $form->id }} - {{ $form->form_language->name }}</option>
          @endforeach
        </select>
        </div>

        <div class="form-group">
          <label for="input9">Özet</label>
          <textarea class="form-control" id="input9" rows="3" placeholder="Özet" name="description">{!! $category ? $category->category_language->description : null !!}</textarea>
        </div>

        <div class="form-group">
          <label for="contents">İçerik</label>
          <textarea name="contents" id="contents" class="ckeditor">{!! $category ? $category->category_language->contents : null !!}</textarea>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-12">
              <label>Tüm Dillerde Ortak Resimler / Dosyalar :</label>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-4">

              <div class="row">
                <div class="col-12">
                      <div class="input-group">
                          <span class="input-group-btn">
                              <a id="lfm" data-input="thumbnail_c_1" data-preview="holder_c_1" class="lfm btn btn-primary text-light d-flex align-items-center justify-content-center">
                                  <i class="fas fa-image mr-2 fa-lg"></i> Seç
                              </a>
                          </span>
                      <input id="thumbnail_c_1" class="form-control" type="text" name="category_image" value="{{ $category ? $category->image : null }}">
                      </div>
                      <div id="holder_c_1" style="margin-top:15px;">
                          <img src="{{ $category ? $category->image : null }}" class="img-fluid" style="max-height: 200px" alt="">
                      </div>
                </div>
              </div>

            </div>

            <div class="col-lg-4">

              <div class="row">
                <div class="col-12">
                      <div class="input-group">
                      <span class="input-group-btn">
                          <a id="lfm" data-input="thumbnail_c_2" data-preview="holder_c_2" class="lfm btn btn-primary text-light d-flex align-items-center justify-content-center">
                              <i class="fas fa-image mr-2 fa-lg"></i> Seç
                          </a>
                      </span>
                      <input id="thumbnail_c_2" class="form-control" type="text" name="category_image2" value="{{ $category ? $category->image2 : null }}">
                      </div>
                      <div id="holder_c_2" style="margin-top:15px;">
                          <img src="{{ $category ? $category->image2 : null }}" class="img-fluid" style="max-height: 200px" alt="">
                      </div>
                </div>
              </div>

            </div>

            <div class="col-lg-4">

              <div class="row">
                <div class="col-12">
                      <div class="input-group">
                          <span class="input-group-btn">
                              <a id="lfm" data-input="thumbnail_c_3" data-preview="holder_c_3" class="lfm btn btn-primary text-light d-flex align-items-center justify-content-center">
                                  <i class="fas fa-image mr-2 fa-lg"></i> Seç
                              </a>
                          </span>
                      <input id="thumbnail_c_3" class="form-control" type="text" name="category_image3" value="{{ $category ? $category->image3 : null }}">
                      </div>
                      <div id="holder_c_3" style="margin-top:15px;">
                          <img src="{{ $category ? $category->image3 : null }}" class="img-fluid" style="max-height: 200px" alt="">
                      </div>
                </div>
              </div>

            </div>

          </div>

        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-12">
              <label>{{ session('lang_name') }} Diline Ait Resimler / Dosyalar :</label>
            </div>
          </div>
          <div class="row">

            <div class="col-lg-4">
              <div class="row">
              <div class="col-12">
                  <div class="input-group">
                      <span class="input-group-btn">
                          <a id="lfm" data-input="thumbnail_cl_1" data-preview="holder_cl_1" class="lfm btn btn-primary text-light d-flex align-items-center justify-content-center">
                              <i class="fas fa-image mr-2 fa-lg"></i> Seç
                          </a>
                      </span>
                  <input id="thumbnail_cl_1" class="form-control" type="text" name="language_image" value="{{ $category ? $category->category_language->image  : null }}">
                  </div>
                  <div id="holder_cl_1" style="margin-top:15px;">
                      <img src="{{ $category ? $category->category_language->image : null }}" class="img-fluid" style="max-height: 200px" alt="">
                  </div>
              </div>
              </div>
            </div>

            <div class="col-lg-4">
              <div class="row">
              <div class="col-12">
                  <div class="input-group">
                      <span class="input-group-btn">
                          <a id="lfm" data-input="thumbnail_cl_2" data-preview="holder_cl_2" class="lfm btn btn-primary text-light d-flex align-items-center justify-content-center">
                              <i class="fas fa-image mr-2 fa-lg"></i> Seç
                          </a>
                      </span>
                  <input id="thumbnail_cl_2" class="form-control" type="text" name="language_image2" value="{{ $category ? $category->category_language->image2 : null }}">
                  </div>
                  <div id="holder_cl_2" style="margin-top:15px;">
                      <img src="{{ $category ? $category->category_language->image2 : null }}" class="img-fluid" style="max-height: 200px" alt="">
                  </div>
              </div>
              </div>
            </div>

            <div class="col-lg-4">
              <div class="row">
              <div class="col-12">
                  <div class="input-group">
                      <span class="input-group-btn">
                          <a id="lfm" data-input="thumbnail_cl_3" data-preview="holder_cl_3" class="lfm btn btn-primary text-light d-flex align-items-center justify-content-center">
                              <i class="fas fa-image mr-2 fa-lg"></i> Seç
                          </a>
                      </span>
                  <input id="thumbnail_cl_3" class="form-control" type="text" name="language_image3" value="{{ $category ? $category->category_language->image3 : null }}">
                  </div>
                  <div id="holder_cl_3" style="margin-top:15px;">
                      <img src="{{ $category ? $category->category_language->image3 : null }}" class="img-fluid" style="max-height: 200px;" alt="">
                  </div>
              </div>
              </div>
            </div>

          </div>

        </div>
</div>
