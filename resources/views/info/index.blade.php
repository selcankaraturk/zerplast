
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.tailwindcss.com"></script>
    <title>{{ $title }}</title>
</head>
<body class="bg-gray-900">
    <div class="w-full h-screen flex justify-center items-center flex-col">
        <div class="bg-white py-4 px-6 border-2 border-indigo-500 rounded-md w-full max-w-lg">
            <h1 class="font-bold text-center text-lg">
                {!! $message !!}
            </h1>
            <i class="block text-center mt-2 text-sm">
                {!! $info !!}
            </i>
        </div>
        <a class="text-md mt-4 border-indigo-200 bg-gray-800 border py-2 px-8 text-center text-white rounded-md" href="{{ url()->previous() }}">< Geri</a>
    </div>
</body>
</html>
