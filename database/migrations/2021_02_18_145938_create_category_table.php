<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->id();
            $table->text('image')->nullable();
            $table->text('image2')->nullable();
            $table->text('image3')->nullable();
            $table->integer('sorted')->nullable();
            $table->integer('create_time')->nullable();
            $table->integer('block_id')->nullable();
            $table->integer('form_id')->nullable();
            $table->integer('status')->nullable();
            $table->integer('update_time')->nullable();
            $table->integer('parent_id')->nullable();
            $table->integer('top_menu')->nullable();
            $table->longText('special')->nullable();
            $table->longText('url')->nullable();
            $table->integer('home_page')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
    }
}
