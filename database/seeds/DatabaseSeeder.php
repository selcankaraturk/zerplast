<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            LanguageSeeder::class,
            CategorySeeder::class,
            CategoryLanguageSeeder::class,
            FormSeeder::class,
            FormLanguageSeeder::class,
            LanguageValueSeeder::class,
            NewsletterSeeder::class,
            SettingsSeeder::class,
            SpecialBlocksSeeder::class,
            TaxonomySeeder::class,
            UserRolesSeeder::class,
        ]);
    }
}
