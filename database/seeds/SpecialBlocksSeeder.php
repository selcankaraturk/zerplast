<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SpecialBlocksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('special_blocks')->insert([
            [
                'name' => 'Sayfalar',
                'block_key' => 'PagesController@index'
            ],
            [
                'name' => 'İletişim',
                'block_key' => 'ContactController@index'
            ],
            [
                'name' => 'Blok',
                'block_key' => 'BlockController@block'
            ]
        ]);
    }
}
