<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryLanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_language')->insert([
            [
                'category_id' => 1,
                'language_slug' => 'tr',
                'name' => 'Menü'
            ],
            [
                'category_id' => 1,
                'language_slug' => 'en',
                'name' => 'Menu'
            ],
            [
                'category_id' => 1,
                'language_slug' => 'fr',
                'name' => 'Menu'
            ]
        ]);
    }
}
