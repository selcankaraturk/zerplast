<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            [
                'language_id' => 'tr',
                'default_lang' => 1
            ],
            [
                'language_id' => 'en',
                'default_lang' => 0
            ],
            [
                'language_id' => 'fr',
                'default_lang' => 0
            ]
        ]);
    }
}
