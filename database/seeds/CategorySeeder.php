<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert([
            'sorted' => 999,
            'create_time' => time(),
            'block_id' => 1,
            'form_id' => 0,
            'status' => 1,
            'parent_id' => 0,
            'top_menu' => 1,
            'home_page' => 0
        ]);
    }
}
