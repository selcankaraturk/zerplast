<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // ilk kullanıcıyı ekle
        DB::table('users')->insert([
            'name' => 'Superuser',
            'email' => 'superuser@admin.com',
            'password' => Hash::make('admin123'),
            'role' => 1
        ]);

        // ilk kullanıcıyı ekle
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('admin123'),
            'role' => 2
        ]);
    }
}
