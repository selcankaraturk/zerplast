<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FormLanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('form_language')->insert([
            [
                'form_id' => 1,
                'language_slug' => 'tr',
                'name' => 'İletişim Formu',
                'description' => 'İletişim Formu'
            ],
            [
                'form_id' => 1,
                'language_slug' => 'en',
                'name' => 'Contact Form',
                'description' => 'Contact Form'
            ],
            [
                'form_id' => 1,
                'language_slug' => 'fr',
                'name' => 'Formulaire de contact',
                'description' => 'Formulaire de contact'
            ]
        ]);
    }
}
