<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguageValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('language_value')->insert([
            [
                'language_slug' => 'tr',
                'language_key' => 'Gönder',
                'language_value' => 'Gönder'
            ],
            [
                'language_slug' => 'en',
                'language_key' => 'Gönder',
                'language_value' => 'SEND'
            ],
            [
                'language_slug' => 'fr',
                'language_key' => 'Gönder',
                'language_value' => 'Envoyer'
            ]
        ]);
    }
}
