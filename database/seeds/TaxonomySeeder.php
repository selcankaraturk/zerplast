<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaxonomySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('taxonomy')->insert([
            [
                'category_id' => 1,
                'slug' => 'menu',
                'language_slug' => 'tr',
                'controller' => 'PagesController@index'
            ],
            [
                'category_id' => 1,
                'slug' => 'menu',
                'language_slug' => 'en',
                'controller' => 'PagesController@index'
            ],
            [
                'category_id' => 1,
                'slug' => 'menu',
                'language_slug' => 'fr',
                'controller' => 'PagesController@index'
            ]
        ]);
    }
}
