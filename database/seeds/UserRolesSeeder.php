<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert([
            [ 'role' => 'superuser', 'redirect' => '/superuser', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'role' => 'admin', 'redirect' => '/admin', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'role' => 'user', 'redirect' => '/user', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ]
        ]);
    }
}
